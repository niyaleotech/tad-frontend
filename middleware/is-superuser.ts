export default function ({ store, redirect }) {
  if (!store.getters["main/isSuperUser"]) {
    return redirect("/")
  }
}
