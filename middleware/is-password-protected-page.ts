import dayjs from "dayjs"
import { RootState } from "~/store"

export default function ({ store, redirect, route }) {
  const passwordProtection: RootState["passwordProtection"] =
    store.getters["passwordProtection"]
  const now = dayjs()

  if (now.isAfter(passwordProtection.expiry)) {
    return redirect(`/password-protected?origin=${route.fullPath}`)
  }
}
