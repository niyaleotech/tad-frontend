export default function ({ store, redirect }) {
  if (!store.getters["forms/isFormCreator"]) {
    return redirect("/")
  }
}
