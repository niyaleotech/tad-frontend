export default function ({ store, redirect }) {
  if (!store.getters["forms/isFormResponder"]) {
    return redirect("/")
  }
}
