appointments = [
  // Continued from the previous list...
  {
    id: 10,
    title: "Appointment with Dominik Lamakani",
    description: "First counseling session.",
    date: "2023-06-25",
    time: "10:00 AM PST",
    duration: "1h",
    status: "pending",
    meetingtype: "in person",
    room: "Room 101",
    notes: [
      {
        date: "2023-06-23",
        title: "Pre-Appointment Note",
        text: "Reviewed client's intake form. Noted concerns about recent loss and expressed emotions. Will explore coping strategies and grief processing during the session.",
      },
      {
        date: "2023-06-24",
        title: "Session Preparation",
        text: "Researched grief counseling techniques and interventions. Prepared session plan to address client's presenting concerns. Will encourage open dialogue and active listening.",
      },
      {
        date: "2023-06-25",
        title: "First Counseling Session",
        text: "Engaged in active listening and empathy during the session. Validated client's emotions and provided a safe space for expression. Discussed the impact of grief on daily life and explored healthy coping mechanisms.",
      },
      {
        date: "2023-06-28",
        title: "Progress Assessment",
        text: "Evaluated client's progress since the last session. Noted improvements in coping skills and emotional regulation. Provided positive reinforcement and encouragement to continue the healing journey.",
      },
      {
        date: "2023-07-02",
        title: "Session Summary",
        text: "Summarized key points discussed in the session. Collaborated with the client to set goals for the upcoming sessions. Provided resources for additional support outside of therapy.",
      },
    ],

    client: {
      id: 2,
      name: "Dominik Lamakani",
      email: "dominik@client.com",
      "Presenting problem/Area of Concern": "Anxiety and stress management",
      concern:
        "Dominik is struggling with anxiety and stress, particularly in social situations. He often feels overwhelmed and finds it challenging to manage his emotions. He is seeking support to develop effective coping strategies and improve his overall well-being.",
      mood_when_booking: "Anxious",
      organization: "Sunset High School",
      account_type: "student",
      image:
        "https://images.generated.photos/Kw9CTzlJUNyC4-Xpq7ClmEe7ZrhmNHvsiuU2tmmEd7g/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy92M18w/MDU0Njc4LmpwZw.jpg",
    },
    professional: {
      id: 2,
      name: "Dr. Julia Stern",
      email: "julia.stern@pro.com",
      image:
        "https://images.generated.photos/mcme4lSIX780mxWCmzadylw3Cvd3E_42wjyrTSFIYl0/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy90cmFu/c3BhcmVudF92My92/M18wMDUyMTQ2LnBu/Zw.png",
      bio: "Clinical Psychologist with a focus on cognitive behavioral therapy.",
      organization: "Laguna Beach Counseling",
      licensenumber: "987654321",
      location: "Laguna Beach, CA",
    },
  },
  {
    id: 15,
    title: "Appointment with Jane Smith",
    description: "Initial assessment session.",
    date: "2023-06-26",
    time: "1:00 PM PST",
    duration: "1h",
    status: "upcoming",
    meetingtype: "in person",
    room: "Room 203",
    notes: [
      {
        date: "2023-06-23",
        title: "Pre-Appointment Note",
        text: "Reviewed client's intake form. Noted concerns about recent loss and expressed emotions. Will explore coping strategies and grief processing during the session.",
      },
      {
        date: "2023-06-24",
        title: "Session Preparation",
        text: "Researched grief counseling techniques and interventions. Prepared session plan to address client's presenting concerns. Will encourage open dialogue and active listening.",
      },
      {
        date: "2023-06-25",
        title: "First Counseling Session",
        text: "Engaged in active listening and empathy during the session. Validated client's emotions and provided a safe space for expression. Discussed the impact of grief on daily life and explored healthy coping mechanisms.",
      },
      {
        date: "2023-06-28",
        title: "Progress Assessment",
        text: "Evaluated client's progress since the last session. Noted improvements in coping skills and emotional regulation. Provided positive reinforcement and encouragement to continue the healing journey.",
      },
      {
        date: "2023-07-02",
        title: "Session Summary",
        text: "Summarized key points discussed in the session. Collaborated with the client to set goals for the upcoming sessions. Provided resources for additional support outside of therapy.",
      },
    ],

    client: {
      id: 7,
      name: "Jane Smith",
      email: "jane.smith@client.com",
      "Presenting problem/Area of Concern": "Depression and mood disorders",
      concern:
        "Jane has been experiencing symptoms of depression, including persistent sadness, lack of interest in activities, and changes in appetite and sleep patterns. She is seeking help to address her emotional well-being, regain motivation, and develop healthy coping strategies.",
      mood_when_booking: "Sad",
      organization: "Maplewood College",
      account_type: "student",
      image:
        "https://images.generated.photos/oyZfQNJqanm1vn0ILEAs5oCoLTnjnuEhZuiYGCOW67k/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy92M18w/MDMxMjcwLmpwZw.jpg",
    },
    professional: {
      id: 7,
      name: "Dr. Brian Johnson",
      email: "brian.johnson@pro.com",
      image:
        "https://images.generated.photos/ygtLHKNmiYuaQDROuCDq1TcUui_xQdzDfVhXqu-qV_g/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy90cmFu/c3BhcmVudF92My92/M18wNjA3MDk3LnBu/Zw.png",
      bio: "Experienced Clinical Psychologist with a focus on family therapy.",
      organization: "Oak Tree Counseling Center",
      licensenumber: "333444555",
      location: "Sacramento, CA",
    },
  },
  {
    id: 11,
    title: "Appointment with Ivan Mesaros",
    description: "Monthly check-in.",
    date: "2023-06-25",
    time: "11:00 AM PST",
    duration: "1h",
    status: "pending cancellation",
    meetingtype: "virtual",
    meetingLink: "https://www.videocall2.com/meeting/ivanmesaros",
    notes: [
      {
        date: "2023-06-23",
        title: "Pre-Appointment Note",
        text: "Reviewed client's intake form. Noted concerns about recent loss and expressed emotions. Will explore coping strategies and grief processing during the session.",
      },
      {
        date: "2023-06-24",
        title: "Session Preparation",
        text: "Researched grief counseling techniques and interventions. Prepared session plan to address client's presenting concerns. Will encourage open dialogue and active listening.",
      },
      {
        date: "2023-06-25",
        title: "First Counseling Session",
        text: "Engaged in active listening and empathy during the session. Validated client's emotions and provided a safe space for expression. Discussed the impact of grief on daily life and explored healthy coping mechanisms.",
      },
      {
        date: "2023-06-28",
        title: "Progress Assessment",
        text: "Evaluated client's progress since the last session. Noted improvements in coping skills and emotional regulation. Provided positive reinforcement and encouragement to continue the healing journey.",
      },
      {
        date: "2023-07-02",
        title: "Session Summary",
        text: "Summarized key points discussed in the session. Collaborated with the client to set goals for the upcoming sessions. Provided resources for additional support outside of therapy.",
      },
    ],

    client: {
      id: 3,
      name: "Ivan Mesaros",
      email: "ivan@client.com",
      "Presenting problem/Area of Concern": "Relationship issues",
      concern:
        "Ivan is facing challenges in his interpersonal relationships, including difficulties with communication and conflict resolution. He seeks guidance to improve his relationship skills, foster healthier connections, and achieve greater satisfaction in his personal life.",
      mood_when_booking: "Neutral",
      organization: "Sunset High School",
      account_type: "student",
      image:
        "https://images.generated.photos/K1kTEwwUOQCp71hiTKfbCOQVX0fpX_fIRM_JYBvW_gc/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy92M18w/MzE1MDE3LmpwZw.jpg",
    },
    professional: {
      id: 3,
      name: "Dr. Matthew Kim",
      email: "matthew.kim@pro.com",
      image:
        "https://images.generated.photos/D8IA0zJxB-Gg8baTOGyw0xYVSCTcj4gMeXi80LQ5E2U/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy90cmFu/c3BhcmVudF92My92/M18wMTQ5ODg0LnBu/Zw.png",
      bio: "Specialist in mental health and well-being techniques.",
      organization: "Sunrise Counselling Center",
      licensenumber: "246813579",
      location: "Santa Monica, CA",
    },
  },
  {
    id: 12,
    title: "Appointment with Maria Martinez",
    description: "Cognitive Behavioral Therapy Session",
    date: "2023-06-25",
    time: "12:00 PM EST",
    duration: "1h",
    status: "upcoming",
    meetingtype: "virtual",
    meetingLink: "https://www.videocall2.com/meeting/mariamartinez",
    notes: [
      {
        date: "06-23-2023",
        title: "Pre-Appointment Note",
        text: "Reviewed client's intake form. Noted concerns about recent loss and expressed emotions. Will explore coping strategies and grief processing during the session.",
      },
      {
        date: "06-24-2023",
        title: "Session Preparation",
        text: "Researched grief counseling techniques and interventions. Prepared session plan to address client's presenting concerns. Will encourage open dialogue and active listening.",
      },
      {
        date: "06-25-2023",
        title: "First Counseling Session",
        text: "Engaged in active listening and empathy during the session. Validated client's emotions and provided a safe space for expression. Discussed the impact of grief on daily life and explored healthy coping mechanisms.",
      },
      {
        date: "06-28-2023",
        title: "Progress Assessment",
        text: "Evaluated client's progress since the last session. Noted improvements in coping skills and emotional regulation. Provided positive reinforcement and encouragement to continue the healing journey.",
      },
      {
        date: "07-02-2023",
        title: "Session Summary",
        text: "Summarized key points discussed in the session. Collaborated with the client to set goals for the upcoming sessions. Provided resources for additional support outside of therapy.",
      },
    ],

    client: {
      id: 4,
      name: "Maria Martinez",
      email: "maria@client.com",
      "Presenting problem/Area of Concern":
        "Stress management and work-life balance",
      concern:
        "Maria is juggling multiple responsibilities and experiencing high levels of stress related to work and personal life. She seeks assistance in developing effective stress management techniques, setting boundaries, and achieving a healthier work-life balance.",
      mood_when_booking: "Stressed",
      organization: "Maplewood College",
      account_type: "student",
      image:
        "https://images.generated.photos/OsjHxJgN5VBJu4L_U6O6ks2o0YP3QBVAZYWJE0fDoVU/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy92M18w/MDY0MDk1LmpwZw.jpg",
    },
    professional: {
      id: 4,
      name: "Dr. Cynthia Wong",
      email: "cynthia.wong@pro.com",
      image:
        "https://images.generated.photos/vXXIylQeglhnxL81OiR0aVs0M_gvtp1GelFAXKxOUK8/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy90cmFu/c3BhcmVudF92My92/M18wMjMzMzM0LnBu/Zw.png",
      bio: "Specializing in Anxiety and Stress Management",
      organization: "Laguna Beach Counseling",
      licensenumber: "999888777",
      location: "Laguna Beach, CA",
    },
  },
  {
    id: 13,
    title: "Appointment with Vicky Jung",
    description: "Mental Health Check-in",
    date: "2023-06-25",
    time: "2:00 PM EST",
    duration: "1h",
    status: "completed",
    meetingtype: "in person",
    room: "Room 102",
    notes: [
      {
        date: "2023-06-23",
        title: "Pre-Appointment Note",
        text: "Reviewed client's intake form. Noted concerns about recent loss and expressed emotions. Will explore coping strategies and grief processing during the session.",
      },
      {
        date: "2023-06-24",
        title: "Session Preparation",
        text: "Researched grief counseling techniques and interventions. Prepared session plan to address client's presenting concerns. Will encourage open dialogue and active listening.",
      },
      {
        date: "2023-06-25",
        title: "First Counseling Session",
        text: "Engaged in active listening and empathy during the session. Validated client's emotions and provided a safe space for expression. Discussed the impact of grief on daily life and explored healthy coping mechanisms.",
      },
      {
        date: "2023-06-28",
        title: "Progress Assessment",
        text: "Evaluated client's progress since the last session. Noted improvements in coping skills and emotional regulation. Provided positive reinforcement and encouragement to continue the healing journey.",
      },
      {
        date: "2023-07-02",
        title: "Session Summary",
        text: "Summarized key points discussed in the session. Collaborated with the client to set goals for the upcoming sessions. Provided resources for additional support outside of therapy.",
      },
    ],

    client: {
      id: 5,
      name: "Vicky Jung",
      email: "vicky@client.com",
      "Presenting problem/Area of Concern": "Self-esteem and body image",
      concern:
        "Vicky struggles with low self-esteem and negative body image, which affects her overall well-being. She desires support to improve her self-image, cultivate self-acceptance, and develop a healthier relationship with her body.",
      mood_when_booking: "Confident",
      organization: "Sunset High School",
      account_type: "student",
      image:
        "https://images.generated.photos/U9aBeANqaVfqd6PfneI3uyeFq-9779LQMmAmst8RH4Y/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy92M18w/MzQyMzg5LmpwZw.jpg",
    },
    professional: {
      id: 5,
      name: "Dr. Michael Lopez",
      email: "michael.lopez@pro.com",
      image:
        "https://images.generated.photos/sfjnWq3hHABLixnG_PfDbUB38dPOgvfQkiFDsXRnjcY/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy90cmFu/c3BhcmVudF92My92/M18wOTM1MTU5LnBu/Zw.png",
      bio: "Clinical Psychologist focusing on children and adolescents.",
      organization: "Palm Tree Counseling Center",
      licensenumber: "111222333",
      location: "Miami, FL",
    },
  },
  {
    id: 14,
    title: "Appointment with Tisho Yanchev",
    description: "First counseling session.",
    date: "2023-06-25",
    time: "3:00 PM EST",
    duration: "1h",
    status: "completed",
    meetingtype: "virtual",
    meetingLink: "https://www.videocall2.com/meeting/tishoyanchev",
    notes: [
      {
        date: "2023-06-23",
        title: "Pre-Appointment Note",
        text: "Reviewed client's intake form. Noted concerns about recent loss and expressed emotions. Will explore coping strategies and grief processing during the session.",
      },
      {
        date: "2023-06-24",
        title: "Session Preparation",
        text: "Researched grief counseling techniques and interventions. Prepared session plan to address client's presenting concerns. Will encourage open dialogue and active listening.",
      },
      {
        date: "2023-06-25",
        title: "First Counseling Session",
        text: "Engaged in active listening and empathy during the session. Validated client's emotions and provided a safe space for expression. Discussed the impact of grief on daily life and explored healthy coping mechanisms.",
      },
      {
        date: "2023-06-28",
        title: "Progress Assessment",
        text: "Evaluated client's progress since the last session. Noted improvements in coping skills and emotional regulation. Provided positive reinforcement and encouragement to continue the healing journey.",
      },
      {
        date: "2023-07-02",
        title: "Session Summary",
        text: "Summarized key points discussed in the session. Collaborated with the client to set goals for the upcoming sessions. Provided resources for additional support outside of therapy.",
      },
    ],

    client: {
      id: 6,
      name: "Tisho Yanchev",
      email: "tisho@client.com",
      "Presenting problem/Area of Concern": "Grief and loss",
      concern:
        "Tisho has recently experienced a significant loss, and he is navigating the complex emotions associated with grief. He seeks support to cope with the loss, process his emotions, and find ways to adjust to life without his loved one.",
      mood_when_booking: "Sad",
      organization: "Maplewood College",
      account_type: "student",
      image:
        "https://images.generated.photos/mdf2IcVMDL0Se6ijF4lYdoQZ50NYmvqXgDlarrlP664/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy92M18w/MzU3MDU5LmpwZw.jpg",
    },
    professional: {
      id: 6,
      name: "Dr. Anita Singh",
      email: "anita.singh@pro.com",
      image:
        "https://images.generated.photos/JBCW0qxw7EW8GXa8xW6MfrhehDa-g7A-i2T5zVcF5Ec/rs:fit:256:256/czM6Ly9pY29uczgu/Z3Bob3Rvcy1wcm9k/LnBob3Rvcy90cmFu/c3BhcmVudF92My92/M18wMDQxNjAwLnBu/Zw.png",
      bio: "A dedicated professional in the field of psychotherapy, working with adults, couples, and families.",
      organization: "Sunset Counseling Services",
      licensenumber: "444555666",
      location: "San Francisco, CA",
    },
  },
]

module.exports = appointments
