import styled from "vue-styled-components"

const props = {
  primary: Boolean,
  secondary: Boolean,
  tertiary: Boolean,
  color: String,
}

export default styled.a`
  color: ${color} !important;
  &:hover {
    opacity: 0.8;
  }
`

export function color(props) {
  if (props.color) return props.color
  if (props.primary) return props.theme.primary
  if (props.secondary) return props.theme.secondary
  if (props.tertiary) return props.theme.tertiary

  return props.theme.secondary
}
