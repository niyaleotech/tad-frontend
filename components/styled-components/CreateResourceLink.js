import styled from "vue-styled-components"

const props = {
  primary: Boolean,
  secondary: Boolean,
  tertiary: Boolean,
  hover: Boolean,
  color: String,
}



export default styled("a", props)`
  background-color: ${bgColor} !important;
`

function bgColor(props) {
  if (props.bg) return props.bg
  if (props.primary) return props.theme.primary
  if (props.secondary) return props.theme.secondary
  if (props.tertiary) return props.theme.tertiary
  if (props.success) return "var(--success-color)"
  if (props.danger) return "var(--danger-color)"
  if (props.warn) return "var(--warn-color)"

  return props.theme.primary
}
