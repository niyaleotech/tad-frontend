import styled from "vue-styled-components"
import { color } from "./TadLink"
import tinycolor from "tinycolor2"

function bg(props) {
  const fg = color(props)
  return tinycolor(fg).setAlpha(0.05)
} 

const SidebarLink = styled.li``
/*
const SidebarLink = styled.li`
  color: ${color} !important;
  &:hover {
    background-color: ${bg} !important;
  }
`
*/

export default SidebarLink
