import styled from "vue-styled-components"
import tinycolor from "tinycolor2"

const props = {
  primary: Boolean,
  secondary: Boolean,
  tertiary: Boolean,
  success: Boolean,
  danger: Boolean,
  warn: Boolean,
  bg: String,
  color: String,
}

function bgColor(props) {
  if (props.bg) return props.bg
  if (props.primary) return props.theme.primary
  if (props.secondary) return props.theme.secondary
  if (props.tertiary) return props.theme.tertiary
  if (props.success) return "var(--success-color)"
  if (props.danger) return "var(--danger-color)"
  if (props.warn) return "var(--warn-color)"

  return props.theme.primary
}

function bgColorHover(props) {
  const bg = bgColor(props)

  return
}

function foregroundBackgroundStyle(props) {
  function fgColor(props, bg) {
    if (props.color) return props.color

    const contrast = tinycolor.readability("#fff", bg)
    // const isDark = tinycolor(bg).isDark()

    // return isDark ? "#fff" : "#424242"

    return contrast > 2 ? "#fff" : "#424242"
  }

  const bg = bgColor(props),
    fg = fgColor(props, bg)

  return {
    "background-color": bg,
    color: fg,
  }
}

function hoverStyle(props) {
  return props.success || props.danger || props.warn
    ? { opacity: 0.9 }
    : { "background-color": bgColorHover(props) }
}

const TadButton = styled("button", props)`
  border: none;
  padding: 0.75rem 1rem;
  font-size: 0.85rem;
  font-weight: 600;
  border-radius: 5px;
  min-width: 100px;
  box-shadow: var(--default-box-shadow);
  margin: 0;
  text-align: center;
  transition: opacity 0.1s ease, transform 0.1s linear;
  ${foregroundBackgroundStyle}

  &:not(:disabled) {
    cursor: pointer;
    &:hover {
      opacity: 0.8;
    }
  }
  &:disabled {
    cursor: not-allowed;
    background-color: rgb(172, 172, 172) !important;
  }
`

export default TadButton

export const OrangeButton = TadButton.extend`
  background-color: #f89e5d;
`
export const PinkButton = TadButton.extend`
  background-color: #ff6b6b;
`
