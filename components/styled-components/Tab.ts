import styled from "vue-styled-components"
import tinycolor from "tinycolor2"

const props = {}

export default styled("li", props)`
  cursor: pointer;
  padding: 1rem;
  &.selected {
    ${foregroundBackgroundStyle};
  }
`

function foregroundBackgroundStyle(props) {
  function fgColor(props, bg) {
    if (props.color) return props.color

    const contrast = tinycolor.readability("#fff", bg)
    return contrast > 2 ? "#fff" : "#424242"
  }

  const bg = props.theme.primary,
    fg = fgColor(props, bg)

  return {
    "background-color": bg,
    color: fg,
  }
}
