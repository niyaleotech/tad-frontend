import styled from "vue-styled-components"

const props = { primary: Boolean, secondary: Boolean, tertiary: Boolean }

export default styled("input", props)`
  display: flex;
  flex-direction: column;
  width: 100%
  margin: 0;
  border-radius: 5px;
  padding: 0.6rem;
  font-size: 1rem;
  background-color: #fff;
  border: 2px solid #f0f0f0;

  &:not(:last-child) {
    margin-bottom: 1.5rem;
  }

  

  &:focus {
    outline: none;
    border-color: ${borderColor};
  }
`
function borderColor(props) {
  if (props.primary) return props.theme.primary
  if (props.secondary) return props.theme.secondary
  if (props.tertiary) return props.theme.tertiary

  return props.theme.primary
}
