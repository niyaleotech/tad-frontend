import styled from "vue-styled-components"

const props = {
  primary: Boolean,
  secondary: Boolean,
  tertiary: Boolean,
  hover: Boolean,
  color: String,
}

export default styled("p", props)`
  color: ${color};
  &:hover {
    opacity: ${(props) => (props.hover ? 0.7 : 1)};
  }
  cursor: ${props.hover ? "pointer" : "default"};
`

export function color(props) {
  if (props.color) return props.color
  if (props.primary) return props.theme.primary
  if (props.secondary) return props.theme.secondary
  if (props.tertiary) return props.theme.tertiary

  return props.theme.primary
}
