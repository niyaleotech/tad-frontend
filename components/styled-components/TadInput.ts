import styled from "vue-styled-components"

const props = { primary: Boolean, secondary: Boolean, tertiary: Boolean }

export default styled("input", props)`
  color: var(--dark-text);
  background-color: #fff;
  &:focus {
    border-color: ${borderColor} !important;
  }
`
function borderColor(props) {
  if (props.primary) return props.theme.primary
  if (props.secondary) return props.theme.secondary
  if (props.tertiary) return props.theme.tertiary

  return props.theme.primary
}
