import Vue from "vue"

export default ({ app }, inject) => {
  Vue.directive("can", {
    inserted: function (el, binding) {
      const permissions = app.store.state.auth.user.permissions
        ? app.store.state.auth.user.permissions
        : [];

      const requiredPermissions = Array.isArray(binding.value) ? binding.value : [binding.value];
      const hasPermission = requiredPermissions.some(permission => permissions.includes(permission));

      if (!hasPermission) {
        el.parentNode.removeChild(el);
      }
    },
  })
}
