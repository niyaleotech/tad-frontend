import Vue from "vue"
import dayjs from "dayjs"
import advancedFormat from "dayjs/plugin/advancedFormat"
import utc from "dayjs/plugin/utc"
import timezone from "dayjs/plugin/timezone"
import { IAppointment, IUserProfile } from "~/interfaces"
dayjs.extend(advancedFormat)
dayjs.extend(utc)
dayjs.extend(timezone)

// Get path to user profile image
Vue.prototype.$userimg = (file: IUserProfile["profile_image"]) =>
  file
    ? `${process.env.apiUrl}/users/file/${file}`
    : "/user-profile-placeholder.png"

Vue.prototype.$usercoverimg = (file: IUserProfile["cover_image"]) =>
  file
    ? `${process.env.apiUrl}/users/download_cover_image/${file}`
    : "/placeholder.png"

// Format date
Vue.prototype.$dateFormat = (date: string | Date, format = "DD MMMM YYYY") => {
  return dayjs.utc(date).tz(dayjs.tz.guess()).format(format)
}

// Ellipsis text
Vue.prototype.$ellipsis = (text: string, max = 50) =>
  text.length <= max ? text : text.substring(0, max - 3).concat("...")

Vue.prototype.$appointmentStartTimezoneSensitive = (
  appointment: IAppointment,
  format = "MMM DD YYYY hh:mm A"
) => dayjs(`${appointment.date}T${appointment.start_time}Z`).format(format)

Vue.prototype.$appointmentEndTimezoneSensitive = (
  appointment: IAppointment,
  format = "MMM DD YYYY hh:mm A"
) => dayjs(`${appointment.date}T${appointment.end_time}Z`).format(format)

// Add to vue interface type so typescript doesn't complain
declare module "vue/types/vue" {
  interface Vue {
    /**
     * Outputs full path to user profile image
     */
    $userimg(file: IUserProfile["profile_image"]): string
    /**
     * Outputs full path to user cover image
     */
    $usercoverimg(file: IUserProfile["cover_image"]): string
    /**
     * Formats given date using dayjs
     */
    $dateFormat(date: string | Date, format?: string): string
    /**
     * Add ellipsis(...) to a string if it exceeds `max` length.
     */
    $ellipsis(text: string, max?: number): string
    /**
     * Timezone-sensitive appointment start time. Use `format` to change the output.
     */
    $appointmentStartTimezoneSensitive(
      appointment: IAppointment,
      format?: string
    ): string
    /**
     * Timezone-sensitive appointment end time. Use `format` to change the output.
     */
    $appointmentEndTimezoneSensitive(
      appointment: IAppointment,
      format?: string
    ): string
  }
}
