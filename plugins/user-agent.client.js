import UAParser from "ua-parser-js"

export default function ({ store }) {
  // Instantiate the UAParser and get the result
  const parser = new UAParser()
  const result = parser.getResult()

  // Extract required information
  const user_agent_info = {
    browser_version: `${result.browser.name} ${result.browser.version}`,
    os_version: `${result.os.name} ${result.os.version}`,
  }

  // Save user_agent_info in the Vuex store
  store.commit("analytics/setUserAgentInfo", user_agent_info)
}
