import axios from "axios"
import { authHeaders } from "."
import { IReferralCreate } from "~/interfaces";

export function sendReferral(data: IReferralCreate) {
  return axios.post(`${process.env.apiUrl}/referrals/`, data)
}

export function getReferrals(options = { skip: 0, limit: 10 }) {
  return axios.get(`${process.env.apiUrl}/referrals/`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

export function addReferralNote(id, note_in) {
  return axios.patch(
    `${process.env.apiUrl}/referrals/add-note/${id}?note_in=${note_in}`,
    {},
    authHeaders()
  )
}
