import axios from "axios"
import { authHeaders } from "."
import {
  IUserCount,
  IUserOpenProfileCreate,
  IUserProfile,
  IUserProfileUpdate,
} from "~/interfaces"

export function createMe(data: IUserOpenProfileCreate) {
  return axios.post(`${process.env.apiUrl}/users/open`, data)
}

export function makeVerifiedPublisher(user_id) {
  return axios.patch(
    `${process.env.apiUrl}/users/toggle-verified-publisher/${user_id}`,
    {},
    authHeaders()
  )
}
export function passwordRecovery(email: string) {
  return axios.post(`${process.env.apiUrl}/password-recovery/${email}`)
}

export function resetPassword(password: string, token: string) {
  return axios.post(`${process.env.apiUrl}/reset-password/`, {
    new_password: password,
    token,
  })
}

export function unVerifyPublisher(user_id) {
  return axios.patch(
    `${process.env.apiUrl}/users/toggle-verified-publisher/${user_id}`,
    {},
    authHeaders()
  )
}

export function getVerifiedPublishers(options = { offset: 0, limit: 10 }) {
  return axios.get(`${process.env.apiUrl}/users/verified-publishers`, {
    headers: authHeaders().headers,
    params: {
      ...options,
    },
  })
}

export function getMe() {
  return axios.get<IUserProfile>(
    `${process.env.apiUrl}/users/me`,
    authHeaders()
  )
}
export function getSingleUser(id) {
  return axios.get<IUserProfile>(
    `${process.env.apiUrl}/users/${id}`,
    authHeaders()
  )
}

export function getProUser(id: string) {
  return axios.get(`${process.env.apiUrl}/users/public/${id}`, authHeaders())
}
export function updateMe(data: IUserProfileUpdate) {
  return axios.put<IUserProfile>(
    `${process.env.apiUrl}/users/me`,
    data,
    authHeaders()
  )
}
export function getUsers(options = { offset: 0, limit: 10 }) {
  return axios.get(`${process.env.apiUrl}/users/`, {
    headers: authHeaders().headers,
    params: {
      ...options,
    },
  })
}

export function getUsersManagement(
  options = { offset: 0, limit: 10 },
  role_name = ""
) {
  return axios.get(`${process.env.apiUrl}/users/`, {
    headers: authHeaders().headers,
    params: {
      ...options,
      role_name,
    },
  })
}

export function getUsersInOrganization(
  organization_id: string,
  options = { skip: 0, limit: 10 }
) {
  return axios.get<IUserProfile[]>(
    `${process.env.apiUrl}/users/organization/${organization_id}`,
    {
      headers: authHeaders().headers,
      params: {
        ...options,
      },
    }
  )
}

export function getUsersCount() {
  return axios.get<IUserCount>(
    `${process.env.apiUrl}/users/count`,
    authHeaders()
  )
}
export function updateUser(userId: string, data: IUserProfileUpdate) {
  return axios.put<IUserProfile>(
    `${process.env.apiUrl}/users/${userId}`,
    data,
    authHeaders()
  )
}
export function createUser(data) {
  return axios.post(`${process.env.apiUrl}/users/`, data, authHeaders())
}

// upload csv to create users
export function uploadUsers(data: FormData) {
  return axios.post(`${process.env.apiUrl}/users/list/upload`, data, authHeaders())
}

export function sendResetEmailsToUsersList(data: FormData) {
  return axios.post(
    `${process.env.apiUrl}/utils/send-reset-emails`,
    data,
    authHeaders()
  )
}

export function uploadUserImage(data: FormData) {
  return axios.post(`${process.env.apiUrl}/users/uploads`, data, authHeaders())
}

export function uploadUserCoverImage(data: FormData) {
  return axios.post(
    `${process.env.apiUrl}/users/upload-cover-image`,
    data,
    authHeaders()
  )
}

export function getProfessionals(
  params: {
    available_date: string
    zip_code: string
    gender: string
  },
  skip = 0,
  limit = 10
) {
  const headers = authHeaders().headers
  return axios.get<IUserProfile[]>(`${process.env.apiUrl}/users/professional`, {
    params: {
      ...params,
      skip,
      limit,
    },
    headers,
  })
}

export function getProfessionalsInDateRange(
  params: {
    zip_code: string
    gender: string
    start_date: string
    end_date: string
  },
  skip = 0,
  limit = 100
) {
  const headers = authHeaders().headers
  return axios.get<IUserProfile[]>(
    `${process.env.apiUrl}/users/professionals/next`,
    {
      params: {
        ...params,
        skip,
        limit,
      },
      headers,
    }
  )
}

export function searchPatients(
  search_string: string,
  options: { skip: number; limit: number } = { skip: 0, limit: 100 }
) {
  return axios.get<IUserProfile[]>(`${process.env.apiUrl}/users/patients`, {
    params: {
      search_string,
      skip: options.skip,
      limit: options.limit,
    },
    headers: authHeaders().headers,
  })
}

export function searchUsers(
  search_string: string,
  options: { offset: number; limit: number } = { offset: 0, limit: 100 }
) {
  return axios.get(`${process.env.apiUrl}/users/all`, {
    params: {
      search_string,
      offset: options.offset,
      limit: options.limit,
    },
    headers: authHeaders().headers,
  })
}
export function searchProfessional(
  search_string: string,
  options: { skip: number; limit: number } = { skip: 0, limit: 100 }
) {
  return axios.get(`${process.env.apiUrl}/users/professionals`, {
    params: {
      search_string,
      skip: options.skip,
      limit: options.limit,
    },
    headers: authHeaders().headers,
  })
}

export async function login(username, password) {
  const params = new URLSearchParams()
  params.append("username", username)
  params.append("password", password)
  return await axios.post(`${process.env.apiUrl}/login/access-token`, params)
}

export function deactiveUser(reason, note, user_id) {
  return axios.post(
    `${process.env.apiUrl}/users/inactive/`,
    { reason, note, user_id },
    authHeaders()
  )
}

export function readInactiveUser(user_id) {
  return axios.get(
    `${process.env.apiUrl}/users/inactive/${user_id}`,
    authHeaders()
  )
}

export function activateUser(user_id) {
  return axios.patch(
    `${process.env.apiUrl}/users/inactive/activate/${user_id}`,
    {},
    authHeaders()
  )
}
