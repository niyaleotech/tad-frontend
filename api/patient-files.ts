import axios from "axios"
import { authHeaders } from "."
import { IAppointment, ICaseNote, IPatientFile } from "~/interfaces"

export type PatientFilesListItem = {
  id: string
  areas_of_concern: string
  created_at: string
  updated_at: string
  treatment_plan: string
  professional_id: string
  patient_id: string
  patient_full_name: string
  patient_age: string
  next_appointment_start_time: string
  next_appointment_date: string
}

export function getPatientFiles(pagination?: { skip: number; limit: number }) {
  const skip =
    pagination && pagination.skip !== undefined ? pagination?.skip : 0
  const limit =
    pagination && pagination.limit !== undefined ? pagination?.limit : 100

  return axios.get<PatientFilesListItem[]>(
    `${process.env.apiUrl}/patientfiles/?skip=${skip}&limit=${limit}`,
    {
      headers: authHeaders().headers,
    }
  )
}

export function updatePatientFile(patientFile: IPatientFile) {
  return axios.put<IPatientFile>(
    `${process.env.apiUrl}/patientfiles/${patientFile.id}`,
    patientFile,
    {
      headers: authHeaders().headers,
    }
  )
}

export function getPatientFile(id: string) {
  return axios.get<IPatientFile>(`${process.env.apiUrl}/patientfiles/${id}`, {
    headers: authHeaders().headers,
  })
}

export function getPatientFileAppointments(
  id: string,
  pagination?: { skip: number; limit: number }
) {
  const skip =
    pagination && pagination.skip !== undefined ? pagination?.skip : 0
  const limit =
    pagination && pagination.limit !== undefined ? pagination?.limit : 10

  return axios.get<IAppointment[]>(
    `${process.env.apiUrl}/patientfiles/${id}/appointments?skip=${skip}&limit=${limit}`,
    {
      headers: authHeaders().headers,
    }
  )
}

export function getPatientFileCaseNotes(
  id: string,
  pagination?: { skip: number; limit: number }
) {
  const skip =
    pagination && pagination.skip !== undefined ? pagination?.skip : 0
  const limit =
    pagination && pagination.limit !== undefined ? pagination?.limit : 10

  return axios.get<ICaseNote[]>(
    `${process.env.apiUrl}/patientfiles/${id}/casenotes?skip=${skip}&limit=${limit}`,
    {
      headers: authHeaders().headers,
    }
  )
}
