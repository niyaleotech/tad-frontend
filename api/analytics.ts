import axios from "axios"
import { IResourceAnalytics } from "~/interfaces"
import { authHeaders } from "."

export function getAppointmentAnalytics() {
  return axios.get(
    `${process.env.apiUrl}/appointments/analytics/`,
    authHeaders()
  )
}

export function getVideoAnalytics(owner_id?: string) {
  return axios.get(`${process.env.apiUrl}/resources/videos/analytics/`, {
    headers: authHeaders().headers,
    params: { owner_id },
  })
}

export function getSingleVideoAnalytics(video_id?: string) {
  return axios.get<IResourceAnalytics>(
    `${process.env.apiUrl}/resources/videos/analytics/`,
    {
      headers: authHeaders().headers,
      params: { video_id },
    }
  )
}

export function getSingleAudioAnalytics(audio_id?: string) {
  return axios.get<IResourceAnalytics>(
    `${process.env.apiUrl}/resources/audios/analytics/`,
    {
      headers: authHeaders().headers,
      params: { audio_id },
    }
  )
}

export function getSingleArticleAnalytics(article_id) {
  return axios.get<IResourceAnalytics>(
    `${process.env.apiUrl}/articles/analytics/`,
    {
      headers: authHeaders().headers,
      params: { article_id },
    }
  )
}

export function getAudioAnalytics(owner_id?: string) {
  return axios.get(`${process.env.apiUrl}/resources/audios/analytics/`, {
    headers: authHeaders().headers,
    params: { owner_id },
  })
}

export function getArticleAnalytics(owner_id?: string) {
  return axios.get(`${process.env.apiUrl}/articles/analytics/`, {
    headers: authHeaders().headers,
    params: { owner_id },
  })
}

export function getLoginAttepts(upto) {
  return axios.get(`${process.env.apiUrl}/analytics/admin/login-vs-failed`, {
    headers: authHeaders().headers,
    params: { upto },
  })
}

// a function that acc
export function getUserLoginAnalytics(
  options = { skip: 0, limit: 10, upto: 0 }
) {
  return axios.get(`${process.env.apiUrl}/analytics/admin/user-login`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

// crisis resources

export function getGetHelpNowStats(options = { skip: 0, limit: 10, upto: 0 }) {
  return axios.get(`${process.env.apiUrl}/analytics/crisis-resources/ghn`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

export function getTopCrisisResources(upto) {
  return axios.get(
    `${process.env.apiUrl}/analytics/crisis-resources/top-resource`,
    {
      headers: authHeaders().headers,
      params: { upto },
    }
  )
}

export function createCrisisResourceInteraction(click_name, view_name, resource_name, organization_id) {
  return axios.post(
    `${process.env.apiUrl}/interactions/`,
    {click_name, view_name, resource_name, organization_id},
    authHeaders()
  )
}

export function getMeVsSomeoneelse() {
  return axios.get(
    `${process.env.apiUrl}/analytics/crisis-resources/me-vs-someone-else`,
    {
      headers: authHeaders().headers,
    }
  )
}

export function getSomeoneelseAction() {
  return axios.get(
    `${process.env.apiUrl}/analytics/crisis-resources/someone-else`,
    {
      headers: authHeaders().headers,
    }
  )
}

// appointments
export function getAvailVsBooked(options = { skip: 0, limit: 10, upto: 0 }) {
  return axios.get(
    `${process.env.apiUrl}/analytics/appointment/availability-vs-booked`,
    {
      headers: authHeaders().headers,
      params: { ...options },
    }
  )
}

export function getAppointments(options = { skip: 0, limit: 10, upto: 0 }) {
  return axios.get(
    `${process.env.apiUrl}/analytics/appointment/scheduled-vs-cancelled`,
    {
      headers: authHeaders().headers,
      params: { ...options },
    }
  )
}

export function getAppointmentMoods(upto) {
  return axios.get(
    `${process.env.apiUrl}/analytics/appointment/appointment-moods`,
    {
      headers: authHeaders().headers,
      params: { upto },
    }
  )
}

export function getAppointmentLength(upto) {
  return axios.get(
    `${process.env.apiUrl}/analytics/appointment/avg-appointment-time`,
    {
      headers: authHeaders().headers,
      params: { upto },
    }
  )
}

export function getUserRatio() {
  return axios.get(
    `${process.env.apiUrl}/analytics/appointment/user-ratio`,
    authHeaders()
  )
}

export function getStudentCounselorRatio() {
  return axios.get(
    `${process.env.apiUrl}/analytics/appointment/user-ratio`,
    authHeaders()
  )
}

export function getAppointmentsByCounselor() {
  return axios.get(
    `${process.env.apiUrl}/analytics/appointment/num-appointments-by-counselors`,
    authHeaders()
  )
}

export function postDeviceDetails(browser_version, os_version) {
  return axios.post(
    `${process.env.apiUrl}/activitylogs/device`,
    { browser_version, os_version },
    authHeaders()
  )
}
export function getDeviceDetails(device_type) {
  return axios.get(
    `${process.env.apiUrl}/analytics/admin/device-details/${device_type}`,
    authHeaders()
  )
}

// resources

// export function getAppointments(options = { skip: 0, limit: 10, upto: 0 }) {
//   return axios.get(
//     `${process.env.apiUrl}/analytics/appointment/scheduled-vs-cancelled`,
//     {
//       headers: authHeaders().headers,
//       params: { ...options },
//     }
//   )
// }

export function getResourceInteractions(
  resource_type,
  interaction_type,
  options = { skip: 0, limit: 10, upto: 0 }
) {
  return axios.get(
    `${process.env.apiUrl}/analytics/resources/${resource_type}/${interaction_type}`,
    {
      headers: authHeaders().headers,
      params: { ...options },
    }
  )
}

export function addSearchTerm(user_id, organization_id, resource_name, name){
  return axios.post(
    `${process.env.apiUrl}/activitylogs/search-log`,
    { user_id, organization_id, resource_name, name },
    authHeaders()
  )
}