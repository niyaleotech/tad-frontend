import axios from "axios"
import { authHeaders } from "."
import { IForm, IFormResponse, IFormResponseAnalytics } from "~/interfaces"

export function createResponse(response: {
  form_id: IFormResponse["form_id"]
  respondent: IFormResponse["respondent"]
  status: IFormResponse["status"]
  responses: IFormResponse["responses"]
}) {
  return axios.post<IFormResponse>(
    `${process.env.apiUrl}/form-response/`,
    response,
    authHeaders()
  )
}

// export function getFormsToRespondTo(options = { size: 5, page : 1 }) {
//   return axios.get<{
//     all_forms: Array<IForm[]>
//     not_responded_to: IForm[]
//     responded_to: IForm[]
//   }>(`${process.env.apiUrl}/form-response/me`, authHeaders(), params: {
//       ...options,
//     },)
// }

export function getFormsToRespondTo(options = { size: 5, page: 1 }) {
  return axios.get(`${process.env.apiUrl}/form-response/me`, {
    headers: authHeaders().headers,
    params: {
      ...options,
    },
  })
}

export function getFormToRespondTo(id) {
  return axios.get(
    `${process.env.apiUrl}/form-response/view/${id}`,
    authHeaders()
  )
}

export function getFormResponseAnalytics(id, options) {
  return axios.get(`${process.env.apiUrl}/form-response/details/${id}`, {
    headers: authHeaders().headers,
    params: {
      ...options,
    },
  })
}

export function getExternalFormToRespondTo(id: IForm["id"], token: string) {
  return axios.get<IForm>(
    `${process.env.apiUrl}/form-response/external/view/${id}/${token}`,
    authHeaders()
  )
}

export function createExternalResponse(
  response: {
    respondent_email: string
    form_id: IFormResponse["form_id"]
    status: IFormResponse["status"]
    responses: IFormResponse["responses"]
  },
  token: string
) {
  return axios.post<IFormResponse>(
    `${process.env.apiUrl}/form-response/external/${token}`,
    response,
    authHeaders()
  )
}

export function getUserResponse(form_id, user_id) {
  return axios.get(
    `${process.env.apiUrl}/form-response/details/${form_id}/${user_id}/user`,
    authHeaders()
  )
}

export function getExternalUserResponse(form_id, email) {
  return axios.get(
    `${process.env.apiUrl}/form-response/details/${form_id}/${email}/external`,
    authHeaders()
  )
}