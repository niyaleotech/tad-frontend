import axios from "axios"
import { authHeaders } from "."
import { ICaseNote } from "~/interfaces"

export type PatientFilesListItem = {
  id: string
  areas_of_concern: string
  created_at: string
  updated_at: string
  treatment_plan: string
  professional_id: string
  patient_id: string
  patient_full_name: string
  patient_age: string
  next_appointment_start_time: string
  next_appointment_date: string
}

export function getCaseNotes() {
  return axios.get<ICaseNote[]>(`${process.env.apiUrl}/casenotes/`, {
    headers: authHeaders().headers,
  })
}

export function getCaseNote(id: string) {
  return axios.get<ICaseNote>(`${process.env.apiUrl}/casenotes/${id}`, {
    headers: authHeaders().headers,
  })
}

export function getCaseNoteByAppointmentId(id: string) {
  return axios.get<
    ICaseNote & {
      patient_file_id: string
    }
  >(`${process.env.apiUrl}/casenotes/appointment/${id}`, {
    headers: authHeaders().headers,
  })
}

export function createCaseNote(caseNote) {
  return axios.post(`${process.env.apiUrl}/casenotes/`, caseNote, {
    headers: authHeaders().headers,
  })
}

export function updateCaseNote(caseNote) {
  return axios.put(
    `${process.env.apiUrl}/casenotes/${caseNote.id}`,
    caseNote,
    {
      headers: authHeaders().headers,
    }
  )
}

export function deleteCaseNoteById(id: string) {
  return axios.patch(`${process.env.apiUrl}/casenotes/archive/${id}`, {
  }, {

    headers: authHeaders().headers,

  })
}
