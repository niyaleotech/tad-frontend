import axios from "axios"
import { authHeaders } from "."

export function shareYourConcern(concerned_for_user_id, request_by, comment) {
  return axios.post(
    `${process.env.apiUrl}/concerns/`,
    { concerned_for_user_id, request_by, comment },
    authHeaders()
  )
}

export function getConcerns(options = { skip: 0, limit: 10 }) {
  return axios.get(`${process.env.apiUrl}/concerns/`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

// Function to patch the concern
export function addConcernNote(id, note_in) {
  return axios.patch(
    `${process.env.apiUrl}/concerns/add-note/${id}?note_in=${note_in}`,
    {},
    authHeaders()
  )
}