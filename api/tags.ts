import axios from "axios"
import { authHeaders } from "."


export function getTags() {
  return axios.get(
    `${process.env.apiUrl}/tags/`,
    authHeaders()
  )
}


export function createTag(name, organization_id) {
  return axios.post(
    `${process.env.apiUrl}/tags/`,
    { name, organization_id },
    authHeaders()
  )
}

export function deleteTag(id) {
  return axios.delete(
    `${process.env.apiUrl}/tags/${id}`,
    authHeaders()
  )
}