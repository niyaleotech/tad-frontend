import axios from "axios"
import { authHeaders } from "."

export function createContact(full_name, email, phone, relationship) {
  return axios.post(
    `${process.env.apiUrl}/safety-team/`,
    { full_name, email, phone, relationship },
    authHeaders()
  )
}

export function createContactAdmin(full_name, email, phone, relationship, user_id) {
  return axios.post(
    `${process.env.apiUrl}/safety-team/admin`,
    { full_name, email, phone, relationship, user_id },
    authHeaders()
  )
}

export function getContacts() {
  return axios.get(`${process.env.apiUrl}/safety-team/`, authHeaders())
}

export function sendRequest(user_id) {
  return axios.post(
    `${process.env.apiUrl}/care-team/`,
    { user_id },
    authHeaders()
  )
}

export function adminAddCareTeam(user_id, professional_id) {
  return axios.post(
    `${process.env.apiUrl}/care-team/admin`,
    { user_id, professional_id},
    authHeaders()
  )
}


export function getSentRequest() {
  return axios.get(
    `${process.env.apiUrl}/care-team/sent-requests`,
    authHeaders()
  )
}

export function getPendingRequest() {
  return axios.get(
    `${process.env.apiUrl}/care-team/pending-requests`,
    authHeaders()
  )
}

export function acceptRequest(id) {
  return axios.patch(
    `${process.env.apiUrl}/care-team/accept/${id}`,
    {},
    authHeaders()
  )
}

export function declineRequest(id) {
  return axios.patch(
    `${process.env.apiUrl}/care-team/decline/${id}`,
    {},
    authHeaders()
  )
}
export function getCareTeam() {
  return axios.get(`${process.env.apiUrl}/care-team/`, authHeaders())
}

export function getCareTeamAdmin(id) {
  return axios.get(`${process.env.apiUrl}/care-team/user/${id}`, authHeaders())
}


export function deleteCareTeam(id) {
  return axios.delete(`${process.env.apiUrl}/care-team/${id}`, authHeaders())
}

export function deleteContact(id) {
  return axios.delete(`${process.env.apiUrl}/safety-team/${id}`, authHeaders())
}

export function getUserContact(id) {
  return axios.get(
    `${process.env.apiUrl}/safety-team/user/${id}`,
    authHeaders()
  )
}
