import axios from "axios"
import { authHeaders } from "."

export function getRoles() {
  return axios.get(`${process.env.apiUrl}/roles/`, authHeaders())
}
