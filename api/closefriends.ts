import axios from "axios"
import { authHeaders } from "."

export function getPendingRequest() {
  return axios.get(
    `${process.env.apiUrl}/users/support/pending-requests`,
    authHeaders()
  )
}

export function getSentRequest() {
  return axios.get(
    `${process.env.apiUrl}/users/support/sent-requests`,
    authHeaders()
  )
}

export function getFriends() {
  return axios.get(`${process.env.apiUrl}/users/support/friends`, authHeaders())
}

// eslint-disable-next-line camelcase
export function sendRequest(user_id) {
  return axios.post(
    `${process.env.apiUrl}/users/support/`,
    { user_id },
    authHeaders()
  )
}

export function acceptRequest(id) {
  return axios.patch(
    `${process.env.apiUrl}/users/support/accept/${id}`,
    {},
    authHeaders()
  )
}

export function declineRequest(id) {
  return axios.patch(
    `${process.env.apiUrl}/users/support/decline/${id}`,
    {},
    authHeaders()
  )
}

export function deleteFriend(id) {
  return axios.delete(
    `${process.env.apiUrl}/users/support/remove-friend/${id}`,
    authHeaders()
  )
}
