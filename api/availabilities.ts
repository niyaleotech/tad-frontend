import axios from "axios"
import { authHeaders } from "."
import {
  IAvailability,
  IAvailabilityCreate,
  IAvailabilityUpdate,
} from "~/interfaces"

export function getAvailabilities(current_date = "") {
  // eslint-disable-next-line camelcase
  const currentDateQuery = current_date
    ? `&availability_date=${current_date}`
    : ""
  return axios.get(
    `${process.env.apiUrl}/users/professional?skip=0${currentDateQuery}`,
    authHeaders()
  )
}

export function getMyAvailabilities(startDate: string, endDate: string) {
  // eslint-disable-next-line camelcase
  return axios.get(
    `${process.env.apiUrl}/availabilities/?start_date=${startDate}&end_date=${endDate}`,
    authHeaders()
  )
}

export function getSingleDay(current_date = "", id: string) {
  // eslint-disable-next-line camelcase
  const currentDateQuery = current_date
    ? `&availability_date=${current_date}`
    : ""
  return axios.get(
    `${process.env.apiUrl}/availabilities/professional/${id}?skip=0&limit=100${currentDateQuery}`,
    authHeaders()
  )
}

export function getPeerAvailabilities() {
  return axios.get<IAvailability[]>(
    `${process.env.apiUrl}/availabilities/2`,
    authHeaders()
  )
}

export function getProfessionalAvailabilities(
  professionalId: string,
  // eslint-disable-next-line camelcase
  skip: number = 10,
  availability_date: string
) {
  return axios.get(
    `${process.env.apiUrl}/availabilities/professional/${professionalId}?offset=${skip}&limit=8`,
    {
      params: { availability_date },
      ...authHeaders(),
    }
  )
}

export function getProfessionalAvailabilitiesInDateRange(
  professionalId: string,
  // eslint-disable-next-line camelcase
  start_date: string,
  // eslint-disable-next-line camelcase
  end_date: string
) {
  return axios.get<IAvailability[]>(
    `${process.env.apiUrl}/availabilities/next/professional/${professionalId}?skip=0&limit=10000`,
    {
      params: { start_date, end_date },
      ...authHeaders(),
    }
  )
}
export function updateAvailability(
  availabilityId: string,
  data: IAvailabilityUpdate
) {
  return axios.put(
    `${process.env.apiUrl}/availabilities/${availabilityId}`,
    data,
    authHeaders()
  )
}
export function createAvailability(data: IAvailabilityCreate) {
  return axios.post(
    `${process.env.apiUrl}/availabilities/`,
    data,
    authHeaders()
  )
}
export function deleteAvailability(availabilityId: string) {
  return axios.delete(
    `${process.env.apiUrl}/availabilities/${availabilityId}`,
    authHeaders()
  )
}

// eslint-disable-next-line camelcase
export function deleteMultipleAvailabilities(availabilities_id_list: string[]) {
  return axios.delete(`${process.env.apiUrl}/availabilities/`, {
    headers: authHeaders().headers,
    data: {
      availabilities_id_list,
    },
  })
}
