import axios from "axios"
import { authHeaders } from "."

export function getAvailableTimeZones() {
  return axios.get(`${process.env.apiUrl}/utils/timezones`, authHeaders())
}

export function sendPasswordRecoveryEmail(email: String) {
  return axios.post(
    `${process.env.apiUrl}/password-recovery/${encodeURIComponent(
      email as any
    )}`
  )
}

export function resetPassword(new_password: string, token: string) {
  return axios.post(
    `${process.env.apiUrl}/reset-password/`,
    { new_password, token },
    authHeaders()
  )
}
