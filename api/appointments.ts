import axios from "axios"
import { authHeaders } from "."
import {
  IAppointment,
  IAppointmentCreate,
  IAppointmentUpdate,
} from "~/interfaces"

export function getAppointment(id: string) {
  return axios.get<IAppointment>(`${process.env.apiUrl}/appointments/${id}`, {
    headers: authHeaders().headers,
  })
}

export function getAppointments(options = { skip: 0, limit: 10 }) {
  return axios.get<IAppointment[]>(`${process.env.apiUrl}/appointments/`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

export function getMyAppointments(options = { offset: 0, limit: 10 }) {
  return axios.get(`${process.env.apiUrl}/appointments/client`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

export function getProfessionalAppointments(options = { offset: 0, limit: 10 }) {
  return axios.get(`${process.env.apiUrl}/appointments/`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

// get appointment by availabiltiy id

export function getAppointmentByAvailabilityId(id: string) {
  return axios.get(
    `${process.env.apiUrl}/appointments/availability/${id}`,
    {
      headers: authHeaders().headers,
    }
  )
}




export function updateAppointment(
  appointmentId: any,
  data: IAppointmentUpdate
) {
  return axios.put(
    `${process.env.apiUrl}/appointments/${appointmentId}`,
    data,
    authHeaders()
  )
}


export function createAppointment(data) {
  return axios.post(`${process.env.apiUrl}/appointments/`, data, authHeaders())
}

export function confirmAppointment(appointmentId: string) {
  return axios.patch(
    `${process.env.apiUrl}/appointments/confirm/${appointmentId}`,
    "",
    authHeaders()
  )
}
export function rejectAppointment(appointmentId: string, reason) {
  return axios.patch(
    `${process.env.apiUrl}/appointments/reject/${appointmentId}?reason=${reason}`,
"",
    authHeaders()
  )
}

export function requestCancellation(appointmentId: string, reason) {
  return axios.patch(
    `${process.env.apiUrl}/appointments/request-cancellation/${appointmentId}?reason=${reason}`,
    {},
    authHeaders()
  )
}
export function completeAppointment(appointmentId: string) {
  return axios.patch(
    `${process.env.apiUrl}/appointments/complete/${appointmentId}`,
    "",
    authHeaders()
  )
}

export function scheduleAppointmentMeeting(appointmentId: string) {
  return axios.patch<IAppointment>(
    `${process.env.apiUrl}/appointments/schedule/${appointmentId}?meeting_provider=jitsi`,
    {},
    authHeaders()
  )
}

export function deleteAppointment(id: string) {
  return axios.delete(`${process.env.apiUrl}/appointments/${id}`, authHeaders())
}
