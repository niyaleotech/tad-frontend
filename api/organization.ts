import axios from "axios"
import { authHeaders } from "."
import { IOrganization } from "~/interfaces"

export function createOrganization(organization: FormData) {
  return axios.post<IOrganization>(
    `${process.env.apiUrl}/organizations/`,
    organization,
    authHeaders()
  )
}

export function getOrganizations() {
  return axios.get<IOrganization[]>(
    `${process.env.apiUrl}/organizations/`,
    authHeaders()
  )
}

export function getOrganization(id: IOrganization["id"]) {
  return axios.get<IOrganization>(
    `${process.env.apiUrl}/organizations/${id}`,
    authHeaders()
  )
}

export function updateOrganization(
  id: IOrganization["id"],
  organization: FormData
) {
  return axios.put<IOrganization>(
    `${process.env.apiUrl}/organizations/${id}`,
    organization,
    authHeaders()
  )
}

export function getOrgFeatures(id) {
  return axios.get(`${process.env.apiUrl}/organizations/${id}/features`, authHeaders())
}


export function getOrgRoles(organization_id) {
  return axios.get(`${process.env.apiUrl}/roles/`, {
    headers: authHeaders().headers,
    params: { organization_id },
  })
}

export function getAllFeatures() {
  return axios.get(`${process.env.apiUrl}/features/`, authHeaders())
}

export function updateOrgFeatures(id, features) {
  return axios.put(`${process.env.apiUrl}/organizations/${id}/features`, features, authHeaders())
}

export function getRoles() {
  return axios.get(`${process.env.apiUrl}/roles/`, authHeaders())
}

export function getRole(id) {
  return axios.get(`${process.env.apiUrl}/roles/${id}`, authHeaders())
}

export function getOrgFeaturesAndPermissions(id) {
  return axios.get(`${process.env.apiUrl}/organizations/${id}/permissions`, authHeaders())
}
export function createRole(role) {
  return axios.post(`${process.env.apiUrl}/roles/`, role, authHeaders())
}

export function updateRole(id, role) {
  return axios.put(`${process.env.apiUrl}/roles/${id}`, role, authHeaders())
}

export function deleteRole(id) {
  return axios.delete(`${process.env.apiUrl}/roles/${id}`, authHeaders())
}



