import axios from "axios"
import { authHeaders } from "."
import { IAudioResource, IVideoResource } from "~/interfaces"

export function uploadVideo(data: FormData) {
  return axios.post(
    `${process.env.apiUrl}/resources/videos`,
    data,
    authHeaders()
  )
}

export function uploadAudio(data: FormData) {
  return axios.post(
    `${process.env.apiUrl}/resources/audios`,
    data,
    authHeaders()
  )
}

export function createLink(formData: FormData) {
  return axios.post(`${process.env.apiUrl}/documents/`, formData, authHeaders())
}

export function getDocuments(search_string?: string, options = { offset: 0, limit: 10 }) {
  return axios.get(
    `${process.env.apiUrl}/documents/?search_string=${search_string}`,
    {
      headers: authHeaders().headers,
      params: { ...options },
    }
  )
}

export function getDocument(id: string) { 
  return axios.get(`${process.env.apiUrl}/documents/${id}`, authHeaders())
}

// get a single video

export function getVideo(id: string) {
  return axios.get(
    `${process.env.apiUrl}/resources/videos/${id}`,
    authHeaders()
  )
}

// get video streaming
export function getVideoStreamUrl(id: string) {
  return axios.get(
    `${process.env.apiUrl}/resources/videos/stream/${id}`,
    authHeaders()
  )
}

// get video feedback

export function getVideos(options = { skip: 0, limit: 10 }) {
  return axios.get<IVideoResource[]>(`${process.env.apiUrl}/resources/videos`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

export function getVideosMe(options = { skip: 0, limit: 10 }) {
  return axios.get<IVideoResource[]>(
    `${process.env.apiUrl}/resources/videos/me`,
    {
      headers: authHeaders().headers,
      params: { ...options },
    }
  )
}

// get a single podcast

export function getAudio(id: string) {
  return axios.get(
    `${process.env.apiUrl}/resources/audios/stream/${id}`,
    authHeaders()
  )
}

export function getAudioTags(id: string) {
  return axios.get(
    `${process.env.apiUrl}/resources/audios/${id}`,
    authHeaders()
  )
}
export function getAudioAnalytics(audio_id) {
  return axios.get(`${process.env.apiUrl}/resources/audios/analytics/`, {
    headers: authHeaders().headers,
    params: {
      ...audio_id,
    },
  })
}

export function getVideoAnalytics(video_id) {
  return axios.get(`${process.env.apiUrl}/resources/videos/analytics/`, {
    headers: authHeaders().headers,
    params: {
      ...video_id,
    },
  })
}

// edit a video

export function updateVideo(data: FormData, id: string) {
  return axios.put(
    `${process.env.apiUrl}/resources/videos/${id}`,
    data,
    authHeaders()
  )
}

// edit a podcast

export function updatePodcast(data: FormData, id: string) {
  return axios.put(
    `${process.env.apiUrl}/resources/audios/${id}`,
    data,
    authHeaders()
  )
}

export function getAudios(options = { skip: 0, limit: 10 }) {
  return axios.get<IAudioResource[]>(`${process.env.apiUrl}/resources/audios`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

export function getAudiosMe(options = { skip: 0, limit: 10 }) {
  return axios.get<IAudioResource[]>(
    `${process.env.apiUrl}/resources/audios/me`,
    {
      headers: authHeaders().headers,
      params: { ...options },
    }
  )
}

export function approveVideo(id: string, formData) {
  return axios.patch(
    `${process.env.apiUrl}/resources/videos/approve/${id}`,
    formData,
    authHeaders()
  )
}

export function approveAudio(id: string, formData) {
  return axios.patch(
    `${process.env.apiUrl}/resources/audios/approve/${id}`,
    formData,
    authHeaders()
  )
}

export function deleteDocument(id: string) {
  return axios.delete(`${process.env.apiUrl}/documents/${id}`, authHeaders())
}

export function deleteVideo(id: string) {
  return axios.delete(
    `${process.env.apiUrl}/resources/videos/${id}`,
    authHeaders()
  )
}
export function declineVideoResource(decline_reason: string, id: string) {
  return axios.patch(
    `${process.env.apiUrl}/resources/videos/decline/${id}`,
    { decline_reason },
    authHeaders()
  )
}

export function declineAudioResource(decline_reason: string, id: string) {
  return axios.patch(
    `${process.env.apiUrl}/resources/audios/decline/${id}`,
    { decline_reason },
    authHeaders()
  )
}

export function deleteAudio(id: string) {
  return axios.delete(
    `${process.env.apiUrl}/resources/audios/${id}`,
    authHeaders()
  )
}

export function getVideoSignedURL(params: { id: string }) {
  return axios.get<string>(
    `${process.env.apiUrl}/resources/videos/stream/${params.id}`,
    authHeaders()
  )
}

export function getAudioSignedURL(params: { id: string }) {
  return axios.get(
    `${process.env.apiUrl}/resources/audios/stream/${params.id}`,
    authHeaders()
  )
}

// analytics
export function createVideoFeedback(comment, video_id) {
  return axios.post(
    `${process.env.apiUrl}/resources/videos/feedback/`,
    { comment, video_id },
    authHeaders()
  )
}

export function createArticleFeedback(comment, article_id) {
  return axios.post(
    `${process.env.apiUrl}/articles/feedback/`,
    { comment, article_id },
    authHeaders()
  )
}

export function createAudioFeedback(comment, audio_id) {
  return axios.post(
    `${process.env.apiUrl}/resources/audios/feedback/`,
    { comment, audio_id },
    authHeaders()
  )
}

export function createVideoLike(video_id) {
  return axios.post(
    `${process.env.apiUrl}/resources/videos/likes/`,
    { video_id },
    authHeaders()
  )
}

export function getVideoFeedback(id: string) {
  return axios.get(
    `${process.env.apiUrl}/resources/videos/feedback/comments/${id}`,
    authHeaders()
  )
}

export function getAudioFeedback(id: string) {
  return axios.get(
    `${process.env.apiUrl}/resources/audios/feedback/comments/${id}`,
    authHeaders()
  )
}

export function getArticleFeedback(id: string) {
  return axios.get(
    `${process.env.apiUrl}/articles/feedback/comments/${id}`,
    authHeaders()
  )
}

export function createAudioLike(audio_id) {
  return axios.post(
    `${process.env.apiUrl}/resources/audios/likes/`,
    { audio_id },
    authHeaders()
  )
}

export function createArticleLike(article_id) {
  return axios.post(
    `${process.env.apiUrl}/articles/likes/`,
    { article_id },
    authHeaders()
  )
}

export function createVideoView(action, video_id) {
  return axios.post(
    `${process.env.apiUrl}/resources/videos/analytics`,
    { action, video_id },
    authHeaders()
  )
}

export function createArticleView(action, article_id) {
  return axios.post(
    `${process.env.apiUrl}/articles/analytics`,
    { action, article_id },
    authHeaders()
  )
}

export function createPodcastView(action, audio_id) {
  return axios.post(
    `${process.env.apiUrl}/resources/audios/analytics`,
    { action, audio_id },
    authHeaders()
  )
}

export function getLikedVideos() {
  return axios.get(
    `${process.env.apiUrl}/resources/videos/likes/`,
    authHeaders()
  )
}

export function getLikedPodcasts() {
  return axios.get(
    `${process.env.apiUrl}/resources/audios/likes/`,
    authHeaders()
  )
}

export function getLikedArticles() {
  return axios.get(`${process.env.apiUrl}/articles/likes/`, authHeaders())
}

export function getProVideos(id: string, options = { skip: 0, limit: 10 }) {
  return axios.get(`${process.env.apiUrl}/resources/videos/users/${id}`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

export function getProPodcasts(id: string, options = { skip: 0, limit: 10 }) {
  return axios.get(`${process.env.apiUrl}/resources/audios/users/${id}`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

export function getProArticles(id: string, options = { skip: 0, limit: 10 }) {
  return axios.get(`${process.env.apiUrl}/articles/users/${id}`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}
export function isAudioLiked(id: string) {
  return axios.get(
    `${process.env.apiUrl}/resources/audios/likes/is-liked/${id}`,
    authHeaders()
  )
}

export function isVideoLiked(id: string) {
  return axios.get(
    `${process.env.apiUrl}/resources/videos/likes/is-liked/${id}`,
    authHeaders()
  )
}

export function isArticleLiked(id: string) {
  return axios.get(
    `${process.env.apiUrl}/articles/likes/is-liked/${id}`,
    authHeaders()
  )
}

export function shareVideo(video_id, friends_id) {
  return axios.post(
    `${process.env.apiUrl}/resources/videos/share/`,
    { video_id, friends_id },
    authHeaders()
  )
}
export function shareAudio(audio_id, friends_id) {
  return axios.post(
    `${process.env.apiUrl}/resources/audios/share/`,
    { audio_id, friends_id },
    authHeaders()
  )
}

export function shareArticle(article_id, friends_id) {
  return axios.post(
    `${process.env.apiUrl}/articles/share/`,
    { article_id, friends_id },
    authHeaders()
  )
}

export function createExternalResources(data: FormData) {
  return axios.post(
    `${process.env.apiUrl}/externalresources/`,
    data,
    authHeaders()
  )
}

export function updateExternalResource(data: FormData, id: string) {
  return axios.put(
    `${process.env.apiUrl}/externalresources/${id}`,
    data,
    authHeaders()
  )
}

export function getExternalResources(options = { skip: 0, limit: 10 }) {
  return axios.get(`${process.env.apiUrl}/externalresources/`, {
    headers: authHeaders().headers,
    params: { ...options },
  })
}

export function getSingleExternalResource(id: string) {
  return axios.get(
    `${process.env.apiUrl}/externalresources/${id}`,
    authHeaders()
  )
}

export function deleteExternalResource(id: string) {
  return axios.delete(
    `${process.env.apiUrl}/externalresources/${id}`,
    authHeaders()
  )
}

export function searchResults( skip, limit, search_string,type, approved, me) {
  return axios.get(`${process.env.apiUrl}/resources/`, {
    headers: authHeaders().headers,
    params: { skip, limit, search_string, type, approved, me },
  })
}
