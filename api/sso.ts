import axios from "axios"
import { authHeaders } from "."

export function ssoLogin(token: string) {
  return axios.post<{
    access_token: string
    token_type: "bearer"
  }>(`${process.env.apiUrl}/sso/login/aad-token`, { token }, authHeaders())
}
