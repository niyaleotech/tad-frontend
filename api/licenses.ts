import axios from "axios"
import { authHeaders } from "."
import { ILicense } from "~/interfaces/license"

export function getLicenses(id) {
  return axios.get<ILicense[]>(
    `${process.env.apiUrl}/licenses/${id}`,
    authHeaders()
  )
}

export function createLicense(number, type, expiration_date) {
  return axios.post(
    `${process.env.apiUrl}/licenses/`,
    { number, type, expiration_date },
    authHeaders()
  )
}


export function verifyLicense(id) {
  return axios.patch(`${process.env.apiUrl}/licenses/verify/${id}`,
  {},
   authHeaders()
   )
}

