import axios from "axios"
import { authHeaders } from "."
import { IInteractionResource } from "~/interfaces"

export function getResources() {
  return axios.get(
    `${process.env.apiUrl}/interactions/settings/resources/`,
    authHeaders()
  )
}

// eslint-disable-next-line camelcase
export function getInteractionResourceByID(interaction_resource_id: string) {
  return axios.get<IInteractionResource>(
    // eslint-disable-next-line camelcase
    `${process.env.apiUrl}/interactions/settings/resources/${interaction_resource_id}`,
    {
      headers: authHeaders().headers,
    }
  )
}

// eslint-disable-next-line camelcase
export function createResources(
  name,
  number,
  description,
  // eslint-disable-next-line camelcase
  button_text,
  // eslint-disable-next-line camelcase
  number_type
) {
  return axios.post(
    `${process.env.apiUrl}/interactions/settings/resources/`,
    { name, number, description, button_text, number_type },
    authHeaders()
  )
}

// eslint-disable-next-line camelcase
export function updateResources(
  name,
  number,
  description,
  // eslint-disable-next-line camelcase
  button_text,
  id,
  // eslint-disable-next-line camelcase
  number_type
) {
  return axios.put(
    `${process.env.apiUrl}/interactions/settings/resources/${id}`,
    { name, number, description, button_text, number_type },
    authHeaders()
  )
}

export function deleteResources(id) {
  return axios.delete(
    `${process.env.apiUrl}/interactions/settings/resources/${id}`,
    authHeaders()
  )
}

export function createInteraction(click_name, view_name, organization_id) {
  return axios.post(
    `${process.env.apiUrl}/interactions/`,
    { click_name, view_name, organization_id },
    authHeaders()
  )
}
