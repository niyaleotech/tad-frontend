import axios from "axios"
import { authHeaders } from "."
import { IActivityLog } from "~/interfaces"

export function getActivityLogs(params?: { skip: number; limit: number }) {
  const p = params ?? { skip: 0, limit: 50 }
  return axios.get<IActivityLog[]>(
    `${process.env.apiUrl}/activitylogs/?skip=${p.skip}&limit=${p.limit}`,
    {
      headers: authHeaders().headers,
    }
  )
}
