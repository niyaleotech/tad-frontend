import axios from "axios"
/* eslint-disable camelcase */
import { authHeaders } from "."
import {
  GenericType,
  IForm,
  IFormPacket,
  IOrgDirectoryUser,
  IFormsResponseObject,
} from "~/interfaces"

export function getMyForms(options: GenericType | null = null) {
  const params = options || {
    size: 5,
    page: 1,
    language: "en",
    status: null,
    type: null,
    search: null,
  }

  params.language = params.language || null
  params.status = params.status || null
  params.type = params.type || null
  params.search = params.search || null

  return axios.get(`${process.env.apiUrl}/forms/me`, {
    headers: authHeaders().headers,
    params,
  })
}

export function getAllForms(
  lang: string | null = null,
  status: string | null = null,
  type: string | null = null
) {
  return axios.get<IFormsResponseObject>(`${process.env.apiUrl}/forms/`, {
    headers: authHeaders().headers,
    params: { lang, status, type },
  })
}

export function getMyFormsDrafts() {
  return axios.get<IForm[]>(
    `${process.env.apiUrl}/forms/me/drafts`,
    authHeaders()
  )
}

export function getForm(id: IForm["id"]) {
  return axios.get<{
    form: IForm
    questions: GenericType[]
  }>(`${process.env.apiUrl}/forms/id/${id}`, authHeaders())
}

export function createForm(form) {
  return axios.post(`${process.env.apiUrl}/forms/`, form, authHeaders())
}

export function shareForm(share: {
  form_id: string
  recipients: string[]
  subject: string
  preferred_submit_date: string
  external: boolean
}) {
  return axios.post<IForm>(
    `${process.env.apiUrl}/forms/share`,
    share,
    authHeaders()
  )
}

export function updateForm(form: IForm) {
  return axios.put<IForm>(
    `${process.env.apiUrl}/forms/${form.id}`,
    form,
    authHeaders()
  )
}

export function updateFormStatus(id: IForm["id"], status: IForm["status"]) {
  return axios.put<IForm>(
    `${process.env.apiUrl}/forms/status/${id}?status=${status}`,
    {},
    authHeaders()
  )
}

// Search organization directory for users
export function getUsersFromOrgDirectory(type: string = "all") {
  return axios.get<IOrgDirectoryUser[]>(
    `${process.env.apiUrl}/users/directory/${type}?skip=0&limit=2000`,
    authHeaders()
  )
}

export function getSingleResponse(form_id: string, user_id: string) {
  return axios.get(
    `${process.env.apiUrl}/form-response/details/${form_id}/${user_id}`,
    authHeaders()
  )
}

// Search organization directory for users
export function searchUsersFromOrgDirectory(q: string) {
  return axios.get<IOrgDirectoryUser[]>(
    `${process.env.apiUrl}/users/directory/search-directory?search_string=${q}`,
    authHeaders()
  )
}

export function getUsersFromOrgDirectoryByRole(
  role_id: string | null,
  search_string: string | null
) {
  let qs: string = "skip=0&limit=2000"

  if (role_id) {
    qs += `&role_id=${role_id}`
  }

  if (search_string) {
    qs += `&search_string=${search_string}`
  }

  return axios.get<IOrgDirectoryUser[]>(
    `${process.env.apiUrl}/users/directory/?${qs}`,
    authHeaders()
  )
}

export function sharePacket(payload: GenericType) {
  return axios.post<IForm>(
    `${process.env.apiUrl}/forms/packet/share`,
    payload,
    authHeaders()
  )
}

export function getFormPackets() {
  return axios.get<IFormPacket[]>(
    `${process.env.apiUrl}/forms/packet/?skip=0&limit=1000`,
    authHeaders()
  )
}

export function getSingleQuestionAnalytics(
  form_id: string,
  question_id: string,
  options = { size: 5, page: 1 }
) {
  return axios.get(
    `${process.env.apiUrl}/form-response/details/${form_id}/${question_id}`,
    {
      headers: authHeaders().headers,
      params: {
        ...options,
      },
    }
  )
}

export function getQuestionChartData(form_id: string, question_id: string, type) {
  return axios.get(
    `${process.env.apiUrl}/form-response/details/${form_id}/${question_id}/chart`,
    {
      headers: authHeaders().headers,
      params: {
        type,
      },
    }
  )
}
