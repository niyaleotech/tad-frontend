import axios from "axios"
import {
  IAppointment,
  IAppointmentCreate,
  IAppointmentUpdate,
  IResource,
  IResourceCreate,
  IResourceUpdate,
  IUserCount,
  IUserProfile,
  IUserProfileUpdate,
  IUserProfileCreate,
  IUserOpenProfileCreate,
  IAvailability,
  IAvailabilityUpdate,
  IAvailabilityCreate,
  INotification,
  IArticle,
  IArticleUpdate,
} from "../interfaces"

function getNuxtAuthLocalToken() {
  return localStorage.getItem("auth._token.local")
}

export function authHeaders() {
  return {
    headers: {
      Authorization: getNuxtAuthLocalToken()!,
    },
  }
}

export const api = {
  async logInGetToken(username: string, password: string) {
    const params = new URLSearchParams()
    params.append("username", username)
    params.append("password", password)
    return await axios.post(
      `${process.env.apiUrl}/api/v1/login/access-token`,
      params
    )
  },

  async getResources(token: string) {
    return await axios.get<IResource[]>(
      `${process.env.apiUrl}/api/v1/resources/`,
      authHeaders()
    )
  },
  async updateResource(
    token: string,
    resourceId: number,
    data: IResourceUpdate
  ) {
    return await axios.put(
      `${process.env.apiUrl}/api/v1/resources/${resourceId}`,
      data,
      authHeaders()
    )
  },
  async createResource(token: string, data: IResourceCreate) {
    return await axios.post(
      `${process.env.apiUrl}/api/v1/resources/`,
      data,
      authHeaders()
    )
  },

  async passwordRecovery(email: string) {
    return await axios.post(
      `${process.env.apiUrl}/api/v1/password-recovery/${email}`
    )
  },
  async resetPassword(password: string, token: string) {
    return await axios.post(`${process.env.apiUrl}/api/v1/reset-password/`, {
      new_password: password,
      token,
    })
  },
}

export function getApiErrorMsg(error: any): string {
  let msg = ""
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    // console.log(error.response.data)
    // console.log(error.response.status)
    // console.log(error.response.headers)
    msg = error.response.data.detail
      ? error.response.data.detail
      : error.response.statusText
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    // console.log(error.request)
    msg = "No response was received from the server."
  } else {
    // Something happened in setting up the request that triggered an Error
    // console.log('Error', error.message)
    msg = error.message
  }

  return msg
}
