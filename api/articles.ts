import axios from "axios"
import { authHeaders } from "."
import { IArticle, IArticleUpdate } from "~/interfaces"

export function getArticles(options = { skip: 0, limit: 100 }) {
  return axios.get<IArticle[]>(`${process.env.apiUrl}/articles/`, {
    headers: authHeaders().headers,
    params: {
      ...options,
    },
  })
}

export function getArticlesMe(options = { skip: 0, limit: 100 }) {
  return axios.get<IArticle[]>(`${process.env.apiUrl}/articles/me`, {
    headers: authHeaders().headers,
    params: {
      ...options,
    },
  })
}

export function getArticle(id: IArticle["id"]) {
  return axios.get<IArticle>(
    `${process.env.apiUrl}/articles/${id}`,
    authHeaders()
  )
}

export function getArticleAnalytics(article_id) {
  return axios.get(`${process.env.apiUrl}/articles/analytics/`, {
    headers: authHeaders().headers,
    params: {
      ...article_id,
    },
  })
}

export function getArticleWithSlug(slug: IArticle["slug"]) {
  return axios.get<IArticle>(
    `${process.env.apiUrl}/articles/slug/${slug}`,
    authHeaders()
  )
}

export function createArticle(article: {
  title: string
  description: string
  body: string
  tags: string[]
  is_draft: true
  target_audience: string[]
}) {
  return axios.post<IArticle>(
    `${process.env.apiUrl}/articles/`,
    article,
    authHeaders()
  )
}

export function updateArticle(id: string, article: IArticleUpdate) {
  return axios.put<IArticle>(
    `${process.env.apiUrl}/articles/${id}`,
    article,
    authHeaders()
  )
}

export function approveArticle(id: string, target_audience) {
  return axios.patch<IArticle>(
    `${process.env.apiUrl}/articles/approve/${id}`,
    { target_audience },
    authHeaders()
  )
}

export function deleteArticle(id: string) {
  return axios.delete(`${process.env.apiUrl}/articles/${id}`, authHeaders())
}

export function uploadArticleThumbnail(articleId: string, formData: FormData) {
  return axios.post(
    `${process.env.apiUrl}/articles/thumbnail/${articleId}`,
    formData,
    authHeaders()
  )
}

export function getTagNames() {
  return axios.get<string[]>(`${process.env.apiUrl}/tags/list`, authHeaders())
}
