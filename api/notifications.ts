import axios from "axios"
import { authHeaders } from "."
import { INotification } from "~/interfaces"

export function getNotifications() {
  return axios.get<INotification[]>(
    `${process.env.apiUrl}/notifications/`,
    authHeaders()
  )
}
export function viewNotification(id: string) {
  return axios.patch(
    `${process.env.apiUrl}/notifications/view/${id}`,
    {},
    authHeaders()
  )
}
