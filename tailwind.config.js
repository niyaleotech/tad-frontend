/** @type {import('tailwindcss').Config} */
module.exports = {
  // Add all possible paths where Tailwind utility classes will be kept
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    extend: {
      width: {
        7: "1.75rem",
      },
      height: {
        7: "1.75rem",
      },
      colors: {
        'gray-600': '#41496b',
      },
    },
  },
  plugins: [],
}
