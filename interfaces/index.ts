/* eslint-disable camelcase */
export type AppointmentMood =
  | ""
  | "Feeling Awful"
  | "Feeling Worse Than Normal"
  | "Feeling Just Average"
  | "Feeling Better Than Normal"
  | "Feeling Great"

export interface GenericType {
  [key: string]: any
}

export interface IOrganization {
  name: string
  address: string
  email: string
  url: string
  primary_color: string
  secondary_color: string
  tertiary_color: string
  school_district: string
  number_of_users: number
  in_house_counselors: number
  hours_needed_per_month: number
  id: string
  logo: string
  alt_logo: string
  created_at: string
  company_terms: string
  company_terms_document_in: any
  company_external_resources: string
  company_external_resources_document_in: any
  updated_at: string
  type: "Company" | "Non-profit" | "School District" | "K-12 School" | "College"
}

export interface IUserStructuredPermission {
  feature_name: string
  permissions: string[]
}

export interface IUserProfile {
  updated_at: string
  bio: string
  zip_code: string
  city: string
  county: string
  state: string
  email: string
  gender: string
  profile_image: string
  cover_image: string
  is_active: boolean
  is_superuser: boolean
  full_name: string
  /*
  type:
    | "patient"
    | "professional"
    | "communication"
    | "staff"
    | "parent"
    | "peer_counselor"
    | "admin"
    | "super"
    | "institution"*/
  id: string
  phone_number: string
  date_of_birth: string
  preferred_pronouns: string
  organization_id: string
  organization?: IOrganization
  created_at: string
  has_setup_account: boolean
  is_verified_publisher: boolean
  role: any
  permissions: string[]
  structured_permissions: IUserStructuredPermission[]
  role_id: string
}

export interface IUserProfileUpdate {
  updated_at?: string
  bio?: string
  zip_code?: string
  city?: string
  county?: string
  state?: string
  email?: string
  gender?: string
  profile_image?: string
  full_name?: string
  password?: string
  phone_number?: string
  is_active?: boolean
  is_superuser?: boolean
  type?: string
  preferred_pronouns?: string
  role_id?: string
}

export interface IUserProfileCreate {
  updated_at?: string
  bio?: string
  zip_code?: string
  city?: string
  county?: string
  state?: string
  email: string
  gender?: string
  profile_image?: string
  full_name?: string
  password?: string
  is_active?: boolean
  is_superuser?: boolean
  type?: string
  organization_id: IUserProfile["organization_id"]
}

export interface IUserOpenProfileCreate {
  email: string
  full_name?: string
  password: string
}

export interface IUserCount {
  entity: string
  count: number
}

export interface IResource {
  title: string
  type: string
  link: string
  id: number
}

export interface IResourceUpdate {
  title?: string
  type?: string
  link?: string
}

export interface IResourceCreate {
  title: string
  type?: string
  link?: string
}

export interface IAppointment {
  patient: IUserProfile
  professional_id: string
  patient_id: string
  date: string
  is_completed: boolean
  is_confirmed: boolean
  is_rejected: boolean
  is_disabled: boolean
  start_time: string
  end_time: string
  id: string
  professional: IUserProfile
  is_accepted: boolean
  meeting_link: string
  mood: AppointmentMood
  updated_at: string
}

export interface IAppointmentUpdate {
  professional_id?: string
  patient_id?: string
  date?: string
  is_completed?: boolean
  is_confirmed?: boolean
  is_rejected?: boolean
  is_disabled?: boolean
  start_time?: string
  end_time?: string
  location?: string
  room_number?: string
}

export interface IAppointmentCreate {
  professional_id: string
  date: string
  patient_id: string
  is_completed?: boolean
  is_confirmed?: boolean
  is_rejected?: boolean
  is_disabled?: boolean
  start_time?: string
  end_time?: string
  mood: AppointmentMood
  organization_id: string
}

export interface IAvailability {
  day_of_week: string
  start_time: string
  end_time: string
  owner_id: string
  id: string
  time_start: string
  time_end: string
  owner: IUserProfile
  is_booked: boolean
}

export interface IAvailabilityUpdate {
  day_of_week?: string
  start_time?: string
  end_time?: string
  owner_id?: string
}

export interface IAvailabilityCreate {
  time_start: string
  time_end: string
  repeat_up_to_date?: string
  repeat_frequency?: string
  time_block?: number
  timezone: string
  organization_id: string
}

export interface INotification {
  generated: string
  content: string
  is_read: boolean
  id: string
  notification_topic: string
  created_at: string
}

export interface IModal {
  title: string
  content: any
  props?: {
    onclose?: (props: GenericType | GenericType[] | any) => void
    [key: string]: any
  }
}

export interface IDocument {
  id: string
  title: string
  description: string
  link: string
  created_at: string
  updated_at: string
}

export interface IArticle {
  id: string
  title: string
  description: string
  body: string
  is_approved: boolean
  is_draft: boolean
  created_at: string
  updated_at: string
  author_id: string
  author: IUserProfile
  tags: string[]
  slug: string
  thumbnail_name: string
  target_audience: string[]
}

export interface IArticleUpdate {
  title: string
  description: string
  tags: string[]
  body: string
  is_approved: boolean
  is_draft: boolean
  slug?: string
  thumbnail_name: string
  target_audience?: string[]
}

// export interface IVideoResource {
//   created_at: string //"2022-03-13T16:26:49.148264"
//   id: string //"9b927961-5bda-4356-a96d-ad7b16b99a2e"
//   is_approved: boolean //true
//   owner_id: string //"4953ee1f-0e05-49fd-8334-2f85f1dcd6cc"
//   patient_id: string //"4953ee1f-0e05-49fd-8334-2f85f1dcd6cc"
//   s3_storage_key: string //"4953ee1f-0e05-49fd-8334-2f85f1dcd6cc/Sample Video-recorded-by-4953ee1f-0e05-49fd-8334-2f85f1dcd6cc.mp4"
//   updated_at: string //"2022-03-13T17:33:41.812871"
//   video_title: string //"Sample Video"
//   video_url: string // "Tad Sample Video.mp4"
// }

export interface IVideoResource {
  title: string
  id: string
  owner_id: string
  created_at: string
  updated_at: string
  is_approved: true
  description: string
  decline_reason: string
  thumbnail_url?: string
  owner: IUserProfile
  tags?: any
}

export interface IAudioResource {
  title: string
  id: string
  owner_id: string
  created_at: string
  updated_at: string
  is_approved: true
  description: string
  thumbnail_url: string
  decline_reason: string
  owner: IUserProfile
}

export interface IPatientFile {
  id: string
  areas_of_concern: string
  created_at: string
  updated_at: string
  treatment_plan: string
  professional_id: string
  patient_id: string
  professional: IUserProfile
  patient: IUserProfile
}

export interface ICaseNote {
  id: string
  narrative: string
  session_format: string
  is_draft: boolean
  appointment: IAppointment
  created_at: string
  updated_at: string
  title?: string
}

export interface IFormQuestion {
  id: string
  type: "open ended" | "multiple choice" | "numerical" | "date" | "time"
  content: string
  notes: string
  has_priority_response: boolean
  priority_response: string[]
  possible_responses: string[]
  has_file: boolean
  file_uri: string[]
  position: number
}

export interface IForm {
  id: string
  name: string
  // slug: string
  description: string
  type:
    | "questionnaire form"
    | "consent form"
    | "checkin form"
    | "time"
    | "open ended"
    | "multiple choice"
    | "numerical"
    | "date"
  status: "draft" | "active" | "removed" | "suspended"
  language: "en" | "es" | "fr"
  created_at: string
  updated_at: string
  author_id: IUserProfile["id"]
  questions: IFormQuestion[]
  preferred_submit_date: string
  number_of_respondents: number
  purpose: string
  respondents_group: "patients"
  selected?: boolean
  form: any
}

export interface IFormCreate {
  name: IForm["name"]
  description: IForm["description"]
  type: IForm["type"]
  language: IForm["language"]
  questions: IFormQuestion[]
  preferred_submit_date: IForm["preferred_submit_date"]
  organization_id: IOrganization["id"]
  target: any
}

export interface QuestionResponse {
  id: string
  answers: string[]
  is_priority_response: boolean
}

export interface IFormResponse {
  id: string
  form_id: string
  respondent: string
  status: "complete" | "incomplete"
  responses: QuestionResponse[]
  created_at: string
  updated_at: string
  author_id: string
}

export interface IFormResponseAnalytics {
  respondents_received: number
  number_of_responses_received: number
  responses: {
    respondent: IUserProfile
    response: IFormResponse
  }[]
}

export interface IInteractionResource {
  id: string
  name: string
  number: string
  description: string
  number_type: "text" | "call"
  created_at: string
  updated_at: string
  button_text: string
}

export interface IResourceAnalytics {
  viewed: number
  shared: number
  liked: number
}

export interface IActivityLog {
  id: string
  user_id: string
  action: string
  created_at: string
}

export interface IResourceFeaturedContent {
  id: any
  title: string
  image: string
  tags: string[]
  link: string
}

export interface IOrgDirectoryUser {
  fullname?: string
  full_name?: string
  email?: string
  utype?: string
  avatarUrl?: string
  selected?: boolean
}

declare global {
  interface Window {
    zeda: any
  }
}

export interface IRole {
  id: string
  name: string
  created_at: string
  updated_at: string
  is_default: string
}

export interface IFormPacket {
  id: string
  name: string
  description: string
  forms: IForm["id"][]
  organization_id: string
}

export interface IConcern {
  id: string
  request_by: string
  concerned_for_user_id: string
  comment: string
  note: string
  organization_id: string
  created_at: string
  updated_at: string
  concerned_for_user: IUserProfile
  concerned_user: IUserProfile
}

export interface IReferral {
  id: string
  student_first_name: string
  student_last_name: string
  student_email?: string
  referrer_relationship: string
  referrer_name: string
  referrer_email: string
  comment?: string
  note?: string
  student_id?: string
  organization_id: string
  created_at: string
  updated_at: string
}

export interface IReferralCreate {
  student_first_name: string
  student_last_name: string
  student_email?: string
  referrer_relationship: string
  referrer_name: string
  referrer_email: string
  comment?: string
  student_id?: string
  organization_id: string
}

export interface IFormsResponseObject {
  items: IForm[]
  page: number
  pages: number
  size: number
  total: number
}
