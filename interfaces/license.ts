export interface ILicense {
  id: string
  number: string
  type: string
  expiration_date: string
  is_verified: boolean
  professional_id: string
  created_at: string
  updated_at: string
}

export interface ILicenseCreate {
  number: string
  type: string
  expiration_date: string
  is_verified?: boolean
  professional_id?: string
}
