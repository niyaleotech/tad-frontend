const path = require('path')

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "TAD Health",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
    ],
    link: [
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap",
      },
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "/vendor/line-awesome/1.3.0/css/line-awesome.min.css",
      },
    ],
  },

  // Env: https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-env/
  env: {
    baseUrl: process.env.VUE_APP_DOMAIN === 'localhost' ? 'http://localhost:3000' : `https://${process.env.VUE_APP_DOMAIN}`,
    appName: process.env.VUE_APP_NAME,
    apiUrl: process.env.VUE_APP_FULL_API_URL,
    fullApiUrl: process.env.VUE_APP_FULL_API_URL,
    organizationId: process.env.ORGANIZATION_ID,
    brandPColor: process.env.BRAND_THEME_P_COLOR,
    brandSColor: process.env.BRAND_THEME_S_COLOR,
    brandTColor: process.env.BRAND_THEME_T_COLOR,
    brandLogo: process.env.BRAND_LOGO,
    brandAltLogo: process.env.BRAND_ALT_LOGO,
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // Resets and normalization
    "@/assets/css/reset.css",
    // Main style sheet loaded here
    "@/assets/css/style.scss",
    "@/assets/css/tailwind.css",
  ],

  script: [],

  image: {
    domains: [`${process.env.VUE_APP_DOMAIN}`],
    alias: {
      tad: `https://${process.env.VUE_APP_DOMAIN}`,
    },
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/vee-validate",
    { src: "~/plugins/v-calendar.ts", mode: "client" },
    "~/plugins/utils.ts",
    "~/plugins/user-agent.client.js",
    "~/plugins/directives.js",
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: { path: "~/components", extensions: ["vue"] },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build",
    "@nuxtjs/pwa",
    // Nuxt 2 only:
    // https://composition-api.nuxtjs.org/getting-started/setup#quick-start
    "@nuxtjs/composition-api/module",
    ["@pinia/nuxt", { disableVuex: false }],
    "@nuxtjs/tailwindcss",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    "@nuxtjs/auth-next",
    // https://go.nuxtjs.dev/pwa
    "@nuxtjs/pwa",
    // https://go.nuxtjs.dev/content
    // "@nuxt/content",
    // https://i18n.nuxtjs.org/
    "nuxt-i18n",
    "@nuxtjs/toast",
    // "@nuxt/image",
  ],

  toast: {
    position: "top-center",
    duration: 5000,
    register: [
      // Register custom toasts
      {
        name: "my-error",
        message: "Oops...Something went wrong",
        options: {
          type: "error",
        },
      },
    ],
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.VUE_APP_FULL_API_URL,
    credentials: true,
  },

  auth: {
    strategies: {
      local: {
        token: {
          property: "access_token",
          global: true,
          // required: true,
          // type: 'Bearer'
        },
        user: {
          property: false,
          // autoFetch: true
        },
        endpoints: {
          login: { url: "/login/access-token", method: "post" },
          logout: false,
          user: { url: "/users/me", method: "get" },
        },
      },
      aad: {
        scheme: "oauth2",
        endpoints: {
          authorization:
            "https://login.microsoftonline.com/tadhealthtest.onmicrosoft.com/oauth2/authorize",
          // authorization:
          //   "https://login.microsoftonline.com/greenwoodcollege.com/oauth2/authorize",

          // token:
          //   "https://login.microsoftonline.com/organizations/oauth2/v2.0/token",
          userInfo: "",
          logout:
            "https://login.microsoftonline.com/tadhealthtest.onmicrosoft.com/oauth2/logout",
          // logout:
          //   "https://login.microsoftonline.com/greenwoodcollege.com/oauth2/logout",
        },
        token: {
          property: "access_token",
          type: "Bearer",
          maxAge: 1800,
        },
        refreshToken: {
          property: "refresh_token",
          maxAge: 60 * 60 * 24 * 30,
        },
        responseType: "id_token",
        grantType: "authorization_code",
        accessType: "offline",
        // ******** change this for your Application (Client) ID ********
        clientId: "7af337d3-b690-430f-83c8-5ec7c550e5f9",
        // clientId: "303d3bbb-351d-4c31-be43-af6a85e5ae6a",
        codeChallengeMethod: "S256",
        scope: [],
        autoLogout: true,
        logoutRedirectUri: "/login",
      },
      google: {
        clientId: process.env.GOOGLE_CLIENT_ID,
        redirectUri: `https://${process.env.VUE_APP_DOMAIN}/sso/callback/`,
        codeChallengeMethod: "",
        responseType: "id_token",
        grantType: "authorization_code",
        accessType: "offline",
        include_granted_scopes: true,
        scope: [],
        autoLogout: true,
        endpoints: {
          userInfo: `${process.env.VUE_APP_FULL_API_URL}/users/me`,
        },
        token: {
          property: "access_token",
          type: "Bearer",
          maxAge: 1800,
        },
        refreshToken: {
          property: "refresh_token",
          maxAge: 60 * 60 * 24 * 30,
        },
      },
    },
    redirect: {
      login: "/login",
      logout: "/login",
      home: "/",
      callback: "/sso/callback",
    },
  },

  // nuxt/i18n module configuration: https://i18n.nuxtjs.org/basic-usage
  i18n: {
    locales: ["en", "fr", "es"],
    defaultLocale: "en",
    vueI18n: {
      fallbackLocale: "en",
      messages: {
        en: {
          welcome: "Welcome",
        },
        fr: {
          welcome: "Bienvenue",
        },
        es: {
          welcome: "Bienvenido",
        },
      },
    },
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: "i18n_redirected",
      onlyOnRoot: true, // recommended
    },
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    icon: false, // disables the icon module
    manifest: {
      name: "TAD Health",
      lang: "en",
    },
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  // content: {
  //   nestedProperties: ["author.name"],
  // },

  storybook: {
    addons: ["@storybook/addon-controls", "@storybook/addon-notes"],
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    loaders: {
      vue: {
        transformAssetUrls: {
          audio: "src",
        },
      },
    },
    extend(config, ctx) {
      if (process.env.NODE_ENV === 'development') {
        config.devtool = 'eval-source-map';
        config.output.devtoolModuleFilenameTemplate = info =>
          info.resourcePath.match(/\.vue$/) && !info.identifier.match(/type=script/)
            ? `webpack-generated:///${info.resourcePath}?${info.hash}`
            : `webpack-tad-health:///${info.resourcePath}`;
        config.output.devtoolFallbackModuleFilenameTemplate = 'webpack:///[resource-path]?[hash]'
      }

      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      })

      const dynamicLoginComponent = process.env.NUXT_ENV_LOGIN_COMPONENT || 'TadHealth' // Has to have NUXT_ENV_ prefix
      config.resolve.alias['TadDynamicLogin'] = path.resolve(__dirname, `components/login/${dynamicLoginComponent}.vue`)
    },
    transpile: ["vee-validate/dist/rules"],
    postcss: {
      plugins: {
        'postcss-import': {},
        tailwindcss: {}
      },
    }
  },
  // Not using SSR
  ssr: false,
  target: "static",
}
