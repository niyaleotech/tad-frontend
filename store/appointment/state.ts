import { IAppointment, IForm } from "@/interfaces"

export interface AppointmentState {
  appointments: IAppointment[]
  selectedForms: IForm[]
}

const defaultState: AppointmentState = {
  appointments: [],
  selectedForms: [],
}

export default () => defaultState
