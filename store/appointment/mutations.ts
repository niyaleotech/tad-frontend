import { IAppointment, IForm } from "@/interfaces"

export default {
  setAppointments(state, payload: IAppointment[]) {
    state.appointments = payload
  },
  selectForm(state, form: IForm) {
    state.selectedForms = [...state.selectedForms, form]
  },

  deselectForm(state, form: IForm) {
    state.selectedForms = [
      ...state.selectedForms.filter((f) => f.id !== form.id),
    ]
  },

  resetForms(state, payload) {
    state.selectedForms = payload
  },
}
