import { RootState } from ".."

export default {
  appointments: (state) => state.appointments,
  oneAppointment: (state) => (appointmentId: number) => {
    const filteredAppointments = state.appointments.filter(
      (appointment) => appointment.id === appointmentId
    )
    if (filteredAppointments.length > 0) {
      return { ...filteredAppointments[0] }
    }
  },
  
  selectedForms(state) {
    return state.selectedForms
  },
}
