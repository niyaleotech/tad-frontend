import { RootState } from ".."

export default {
  isSuperUser: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.is_superuser &&
      rootState.auth.user.is_active
    )
  },
  canViewInteractionResources: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "interaction resources"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewResources: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => 
          structPerm.feature_name === "audios" ||
          structPerm.feature_name === "combined resources" ||
          structPerm.feature_name === "external resources" ||
          structPerm.feature_name === "videos" || 
          structPerm.feature_name === "articles"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewAppointments: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "appointments"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewFormResponses: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "form responses"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewForms: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "forms"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewNotes: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "case notes"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewDocuments: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "documents"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewConcerns: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "concerns"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewUsers: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "users"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewAnalytics: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "analytics"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewOrganization: (state, getters, rootState: RootState, rootGetters) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "manage self organization"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  canViewAllOrganizations: (
    state,
    getters,
    rootState: RootState,
    rootGetters
  ) => {
    return (
      rootState.auth.user &&
      rootState.auth.user.structured_permissions.filter(
        (structPerm) => structPerm.feature_name === "manage organizations"
      ).length >= 1 &&
      rootState.auth.user.is_active
    )
  },
  loginError: (state) => state.logInError,
  dashboardShowDrawer: (state) => state.dashboardShowDrawer,
  dashboardMiniDrawer: (state) => state.dashboardMiniDrawer,
  // userProfile: (state) => state.userProfile,
  // token: (state) => state.token,
  // isLoggedIn: (state) => state.isLoggedIn,
  firstNotification: (state) =>
    state.notifications.length > 0 && state.notifications[0],
}
