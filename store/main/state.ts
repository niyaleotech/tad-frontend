import { IUserCount } from "@/interfaces"

export interface AppNotification {
  content: string
  color?: string
  showProgress?: boolean
}

export interface MainState {
  token: string
  // isLoggedIn: boolean | localStorage.getItem('token') ?
  // isLoggedIn: boolean | null
  logInError: boolean
  // userProfile: IUserProfile | null
  userCount: IUserCount | null
  notifications: AppNotification[]
}

const defaultState: MainState = {
  // isLoggedIn: null,
  token: "",
  logInError: false,
  // userProfile: null,
  userCount: null,
  notifications: [],
}

export default () => defaultState
