// import { getLocalToken, removeLocalToken, saveLocalToken } from "@/utils"
import { AxiosError } from "axios"
import { AppNotification } from "./state"
import { api } from "@/api"

export default {
  async checkApiError({ dispatch }, payload: AxiosError) {
    // console.log(payload.response)
    if (payload.response && payload.response.status === 401) {
      await dispatch("logOut")
    }
  },
  async removeNotification(
    { commit },
    payload: { notification: AppNotification; timeout: number }
  ) {
    return await new Promise((resolve) => {
      setTimeout(() => {
        commit("removeNotification", payload.notification)
        resolve(true)
      }, payload.timeout)
    })
  },
  async passwordRecovery({ commit, dispatch }, payload: { username: string }) {
    try {
      await Promise.all([
        api.passwordRecovery(payload.username),
        await new Promise<void>((resolve) => setTimeout(() => resolve(), 500)),
      ])
    } catch (error) {}
    // Refactored this ... shouldn't give user indication if their attempt was successful or not
    await dispatch("logOut")
    const loadingNotification = {
      content: "Sending password recovery email",
      showProgress: true,
    }
    await commit("addNotification", loadingNotification)
  },
  async resetPassword(
    { commit, dispatch },
    payload: { password: string; token: string }
  ) {
    const loadingNotification = {
      content: "Resetting password",
      showProgress: true,
    }
    try {
      await commit("addNotification", loadingNotification)
      await Promise.all([
        api.resetPassword(payload.password, payload.token),
        await new Promise<void>((resolve) => setTimeout(() => resolve(), 500)),
      ])
      await commit("removeNotification", loadingNotification)
      await commit("addNotification", {
        content: "Password successfully reset",
        color: "success",
      })
      await dispatch("logOut")
    } catch (error) {
      await commit("removeNotification", loadingNotification)
      await commit("addNotification", {
        color: "error",
        content: "Error resetting password",
      })
    }
  },
}
