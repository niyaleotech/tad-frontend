import _ from "lodash"
import { IForm, IModal, IOrgDirectoryUser } from "~/interfaces"

export interface ModalState {
  modal: IModal
  childModal: IModal
  selectedDirectory: IOrgDirectoryUser[]
  selectedForms: IForm[]
}

export const state = (): ModalState => ({
  modal: {} as IModal,
  childModal: {} as IModal,
  selectedDirectory: [] as IOrgDirectoryUser[],
  selectedForms: [] as IForm[],
})

export const getters = {
  getModal(state: ModalState) {
    return state.modal
  },

  getModals(state: ModalState) {
    return {
      modal: state.modal,
      childModal: state.childModal,
    }
  },

  getSelectedDirectory(state: ModalState) {
    return _.uniqBy(state.selectedDirectory, "email")
  },

  getSelectedForms(state: ModalState) {
    return state.selectedForms
  },
}

export const actions = {
  addManyDirUsers({ commit }, users: IOrgDirectoryUser[]) {
    users.map((user) => {
      return commit("addDirUser", user)
    })
  },

  removeManyDirUsers({ commit }, users: IOrgDirectoryUser[]) {
    users.map((user) => {
      return commit("removeDirUser", user)
    })
  },

  addManyForms({ commit }, forms: IForm[]) {
    forms.map((form) => {
      return commit("addForm", form)
    })
  },

  removeManyForms({ commit }, forms: IForm[]) {
    forms.map((form) => {
      return commit("removeForm", form)
    })
  },
}

export const mutations = {
  showModal(state: ModalState, payload: IModal) {
    state.modal = payload
  },

  hideModal(state: ModalState, props: Object | Object[]) {
    if (state.childModal.props) {
      if (state.childModal.props.onclose) state.childModal.props.onclose(props)
      state.childModal = {} as IModal
      return
    }

    if (state.modal.props && state.modal.props.onclose)
      state.modal.props.onclose(props)
    state.modal = {} as IModal
  },

  showChildModal(state: ModalState, payload: IModal) {
    state.childModal = payload
  },

  addDirUser(state: ModalState, user: IOrgDirectoryUser) {
    state.selectedDirectory = [...state.selectedDirectory, user]
  },

  removeDirUser(state: ModalState, user: IOrgDirectoryUser) {
    state.selectedDirectory = state.selectedDirectory.filter(
      (_user) => _user.email !== user.email
    )
  },

  addForm(state: ModalState, form: IForm) {
    state.selectedForms = [...state.selectedForms, form]
  },

  removeForm(state: ModalState, form: IForm) {
    state.selectedForms = state.selectedForms.filter(
      (_form) => _form.id !== form.id
    )
  },

  resetOrgDirectorySelections(state: ModalState, payload) {
    state.selectedDirectory = payload
  },

  resetFormSelections(state: ModalState, payload) {
    state.selectedForms = payload
  },
}
