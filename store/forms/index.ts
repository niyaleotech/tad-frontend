/* eslint-disable @typescript-eslint/no-unused-vars */
import { RootState } from ".."
import { IForm, IOrgDirectoryUser } from "~/interfaces"

export interface FormState {
  packetRecipients: IOrgDirectoryUser[]
  formSelections: IForm[]
}

export const state = (): FormState => ({
  packetRecipients: [] as IOrgDirectoryUser[],
  formSelections: [] as IForm[],
})

export const getters = {
  isFormResponder(state: any, getters: any, rootState: RootState, rootGetters) {
    return rootGetters["main/canViewFormResponses"]
  },

  isFormCreator(state: any, getters: any, rootState: RootState, rootGetters) {
    return rootGetters["main/canViewForms"]
  },

  getPacketRecipients(state: FormState) {
    return state.packetRecipients
  },

  getFormSelections(state: FormState) {
    return state.formSelections
  },
}

export const mutations = {
  addRecipient(state: FormState, user: IOrgDirectoryUser) {
    const exists = state.packetRecipients.filter(
      (_user) => user.email === _user.email
    )

    if (!exists.length) {
      state.packetRecipients = [...state.packetRecipients, user]
    }
  },

  removeRecipient(state: FormState, user: IOrgDirectoryUser) {
    state.packetRecipients = state.packetRecipients.filter(
      (_user) => user.email !== _user.email
    )
  },

  addForm(state: FormState, form: IForm) {
    const exists = state.formSelections.filter((_form) => form.id === _form.id)

    if (!exists.length) {
      state.formSelections = [...state.formSelections, form]
    }
  },

  removeForm(state: FormState, form: IForm) {
    state.formSelections = state.formSelections.filter(
      (_form) => form.id !== _form.id
    )
  },

  resetFormSelections(state: FormState, payload) {
    state.formSelections = payload
  },

  resetOrgDirectorySelections(state: FormState, payload) {
    state.packetRecipients = payload
  },
}
