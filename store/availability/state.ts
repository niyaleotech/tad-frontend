import { IAvailability } from "@/interfaces"

export interface AvailabilityState {
  availabilities: IAvailability[]
}

const defaultState: AvailabilityState = {
  availabilities: [],
}

export default () => defaultState
