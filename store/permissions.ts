import { GetterTree, MutationTree } from "vuex"
import { RootState } from "~/store"

export interface UserState {
  permissions: string[] | null
}

export const state = (): UserState => ({
  permissions: null,
})

export const getters: GetterTree<UserState, RootState> = {
  permissions: (state) => state.permissions,
}

export const mutations: MutationTree<UserState> = {
  setPermissions(state, permissions: string[]) {
    state.permissions = permissions
  },
}


