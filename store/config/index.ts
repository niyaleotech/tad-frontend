import { IOrganization } from "~/interfaces"

export interface ConfigState {
  p_color: IOrganization["primary_color"]
  s_color: IOrganization["secondary_color"]
  t_color: IOrganization["tertiary_color"]
  logo: IOrganization["logo"]
  alt_logo: IOrganization["alt_logo"]
  theme_ready: boolean
}

// Default state
export const state = (): ConfigState => ({
  p_color: process.env.brandPColor || '#1a535c', 
  s_color: process.env.brandSColor || '#1a535c', 
  t_color: process.env.brandTColor || '#1a535c', 
  logo: process.env.brandLogo || "/tadhealth_logo.png",
  alt_logo: process.env.brandAltLogo || "/tadhealth_alt_logo.png",
  theme_ready: false
})

export const getters = {
  themeReady(state: ConfigState) {
    return state.theme_ready
  },
  pColor(state: ConfigState) {
    return state.p_color
  },
  sColor(state: ConfigState) {
    return state.s_color
  },
  tColor(state: ConfigState) {
    return state.t_color
  },
  logo(state: ConfigState) {
    return state.logo
  },
  alt_logo(state: ConfigState) {
    return state.alt_logo
  },
}

export const mutations = {
  setPColor(state: ConfigState, payload: IOrganization["primary_color"]) {
    state.p_color = payload
  },
  setSColor(state: ConfigState, payload: IOrganization["secondary_color"]) {
    state.s_color = payload
  },
  setTColor(state: ConfigState, payload: IOrganization["tertiary_color"]) {
    state.t_color = payload
  },
  setLogo(state: ConfigState, payload: IOrganization["logo"]) {
    state.logo = payload
  },
  setAltLogo(state: ConfigState, payload: IOrganization["alt_logo"]) {
    state.alt_logo = payload
  },
  setThemeReady(state: ConfigState, payload: boolean) {
    state.theme_ready = payload
  },
}
