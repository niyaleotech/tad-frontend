import dayjs from "dayjs"
import { IUserProfile } from "~/interfaces"
import { UserState } from "~/store/permissions"

export interface RootState {
  brandColorDefault: string
  auth: {
    loggedIn: boolean
    user: IUserProfile | false
  }
  passwordProtection: {
    expiry: string
  }
  permissions: UserState // Add this line
}
export const state = (): RootState => ({
  brandColorDefault: "#1a535c",
  auth: {
    loggedIn: false,
    user: false,
  },
  passwordProtection: {
    expiry: dayjs().toISOString(),
  },
  permissions: {
    permissions: null, // Add this line
  },
})

export const getters = {
  isAuthenticated(state: RootState) {
    return state.auth.loggedIn
  },

  loggedInUser(state: RootState) {
    return state.auth.user
  },
  passwordProtection(state: RootState) {
    return state.passwordProtection
  },
}

export const mutations = {
  setPasswordProtection(state: RootState, expiry: string) {
    state.passwordProtection.expiry = expiry
  },
}
