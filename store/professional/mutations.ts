import { IUserProfile } from "@/interfaces"

export default {
  setProfessionalUsers(state, payload: IUserProfile[]) {
    state.professionalUsers = payload
  },
  setProfessionalUser(state, payload: IUserProfile) {
    const professionalUsers = state.professionalUsers.filter(
      (user: IUserProfile) => user.id !== payload.id
    )
    professionalUsers.push(payload)
    state.professionalUsers = professionalUsers
  },
  setProfessionalSelectedUsers(state, payload: IUserProfile) {
    const professionalUsers = state.professionalUsers.filter(
      (user: IUserProfile) => user.gender === payload.gender
    )
    professionalUsers.push(payload)
    state.professionalUsers = professionalUsers
  },
}
