export default {
  professionalUsers: (state) => state.professionalUsers,
  professionalOneUser: (state) => (userId: string) => {
    const filteredUsers = state.professionalUsers.filter(
      (user) => user.id === userId
    )
    if (filteredUsers.length > 0) {
      return { ...filteredUsers[0] }
    }
  },
}
