import { IUserProfile } from "@/interfaces"

export interface ProfessionalState {
  professionalUsers: IUserProfile[]
}

const defaultState: ProfessionalState = {
  professionalUsers: [],
}

export default () => defaultState
