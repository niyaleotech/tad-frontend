import { GetterTree, MutationTree } from "vuex"
import { RootState } from "~/store"

export interface AnalyticsState {
  user_agent_info: {
    browser: string
    version: string
    os: string
    platform: string
  } | null
}

export const state = (): AnalyticsState => ({
  user_agent_info: null,
})

export const mutations: MutationTree<AnalyticsState> = {
  setUserAgentInfo(state, payload) {
    state.user_agent_info = payload
  },
}

export const getters: GetterTree<AnalyticsState, RootState> = {
  userAgentInfo: (state) => state.user_agent_info,
}
