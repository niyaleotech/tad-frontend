// Review global namespacing https://vuex.vuejs.org/guide/modules.html

// export default {
//   async getNotifications({ commit, dispatch, rootState }) {
//     try {
//       const response = await api.getNotifications(rootState.main.token)
//       if (response) {
//         await commit("setNotifications", response.data)
//       }
//     } catch (error) {
//       await dispatch("main/checkApiError", error, { root: true })
//     }
//   },
//   async viewNotification(
//     { commit, dispatch, rootState },
//     payload: { id: string }
//   ) {
//     try {
//       const loadingNotification = { content: "Saving...", showProgress: true }
//       await commit("main/addNotification", loadingNotification)
//       const response = (
//         await Promise.all([
//           api.viewNotification(rootState.main.token, payload.id),
//           await new Promise<void>((resolve) =>
//             setTimeout(() => resolve(), 500)
//           ),
//         ])
//       )[0]
//       await commit("setNotification", response.data)
//       await commit("main/removeNotification", loadingNotification)
//       await commit("main/addNotification", {
//         content: "Profile successfully updated",
//         color: "success",
//       })
//     } catch (error) {
//       await dispatch("main/checkApiError", error)
//     }
//   },
// }
