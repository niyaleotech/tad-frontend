import { INotification } from "@/interfaces"

export interface NotificationState {
  notifications: INotification[]
}

const defaultState: NotificationState = {
  notifications: [],
}

export default () => defaultState
