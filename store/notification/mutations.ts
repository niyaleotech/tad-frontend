import { INotification } from "@/interfaces"

export default {
  setNotifications(state, payload: INotification[]) {
    state.notifications = payload
  },
  setNotification(state, payload: INotification) {
    const notifications = state.notifications.filter(
      (user: INotification) => user.id !== payload.id
    )
    notifications.push(payload)
    state.notifications = notifications
  },
}
