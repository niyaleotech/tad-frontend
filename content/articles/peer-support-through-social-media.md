---
title: Peer Support through Social Media
description: Social media has become an increasingly popular outlet for people suffering with mental health
img: video_one.jpg
alt: peer support
author:
  name: Dr
  bio: All about Dr
  img: video_one.jpg
tags:
  - mental health
---

Social media has become an increasingly popular outlet for people suffering with mental health challenges. Platforms such as Facebook, Twitter, and YouTube, allow people to share their experiences as well as receive advice from others with similar mental health conditions. We believe that online peer-to-peer support can be very beneficial for those with mental health challenges who feel they don’t have an in-person support system. Online peer support gives people the opportunity to connect with others, challenge stigmas, and increase our ideas for formal mental health care.

Creating connections and feeling like one belongs to a group are both very important. Identifying with a social group is believed to increase self-esteem and self-efficacy (the belief that one can succeed and become better), and reduce uncertainty about oneself. This idea is even more crucial in the case of people with mental health challenges, and it has been shown that connecting with others leads to better recovery and personal wellbeing. It also allows people to take a more anonymous stance versus sharing their experiences in person and which can often be uncomfortable. People online can choose their level of engagement and the extent to which they interact with others, which is not always the case in person. For someone experiencing mental health symptoms combined with fear of stigma and rejection, this added control through online communication may be empowering (Source).

Besides the social benefits online peer-to-peer support has on mental health, this type of support gives people the ability to challenge stigmas. Individuals who visit online communities may have fears about reaching out to others, driven by concerns about what other people will say or think and the possibility of rejection because of their illness. Many of the people in these online communities have also or are currently facing the same struggles that the person seeking support is going through. Studies have shown that knowing that there are others facing similar concerns and frustrations can be highly reassuring and can create a sense of belonging to a group. Furthermore, research suggests that marginalized individuals may benefit from feelings of empowerment, greater personal identity and pride by connecting with similar others online (Source).

Online support can increase mental health care seeking behaviors. A recent study found that many people with mental illness were motivated to seek formal mental health care after first searching or discussing concerns with peers online. Prior studies have also shown that when someone learns about other people’s personal experiences facing illness, they feel more confident and empowered in making their own healthcare decisions. Learning from peers through online networks can help individuals realize they can make their own healthcare decisions, prepare them for medical visits, and empower them to be proactive in their communication with healthcare providers.

Talk. Share. Help.
