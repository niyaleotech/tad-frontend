---
title: Peer vs. Clinical Support
description: Peer support has become an increasingly popular source of help for individuals
img: video_two.jpg
alt: peer vs clinical support
author:
  name: Dr
  bio: Bio
  img: video_two.jpg
tags:
  - mental health
---

Peer support has become an increasingly popular source of help for individuals with mental illnesses. While the main type of peer support comes from people who have themselves suffered with mental illness, peer support can also be beneficial when coming from friends who may not have gone through similar struggles.

Peer support and clinical support have shown to be fairly equal in outcomes of re-hospitalization and relapse, but peer support has shown better outcomes in the recovery process. Specifically, peer support tends to increase levels of empowerment and self-efficacy (the belief in one’s ability to become better). This is mainly due to the social connectedness that comes from interacting with peers, versus the lack thereof in a clinical setting (Source). The exchange of various strategies used to cope with daily challenges of living with mental illness is a crucial aspect of the peer-to-peer support community, and is a great advantage to clinical support. Additionally, peer support builds confidence and hope for healing. Being a part of a community, whether that just be a caring friend or a larger group of people with similar mental health experiences, allows for greater social connections and empowerment than clinical support can provide.

A major advantage of peer support is that everyone involved is equal. Sometimes, having a ‘professional’ in the group can be daunting and diminishing. Peers can often form a stronger therapeutic bond with peers especially if they have experienced mental health struggles themselves. Besides genuine empathy, they are also able to promote treatment through personal empowerment and by becoming a role model for recovery. Even if peers don’t share the same mental health experiences, there is still a lot of value in that connection. People really feel better when they know they are not alone and that other people understand and share their experiences.

Although clinical support has tremendous benefits, coexisting together peer support completes the puzzle for what someone with mental health challenges needs. Therefore, peer support is a valuable asset for individuals with mental illnesses and to have more substantial research in this area is crucial in order to make peer support a more well established practice. Resources, connection, and community allow for building mental health equity and at Talk About Depression we are encouraging this model for expanding the range of peer support available.
