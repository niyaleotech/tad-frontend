---
title: Andrew M's Story
description: My mental health journey started in the middle of high school.
img: video_five.jpg
alt: andrew m
author:
  name: Dr
  bio: All about Dr
  img: video_five.jpg
tags:
  - mental health
---


Content Advisory: Please note the following content can be directly or indirectly related to content about mental health, depression, suicide, and or self-harm.



My mental health journey started in the middle of high school. After suffering a multitude of sports injuries and surgeries that left me sidelined from my friends and activities I loved, I poured all my energy into my academics and extracurriculars. Trying to avoid my feelings of missing out, and feeling frustrated with my family situation, I buried my emotions. I got by, never super happy, always chasing the next step of moving onto college and leaving high school behind.

Once I left high school and moved across the country for college, I found that I didn’t fit into this elite small private college, that I had worked so hard to get into. It felt like everyone already knew each other and had connections either to the area or the college. I questioned why I was even there. Again I followed the same strategy of taking hard classes and filling my plate with numerous extracurriculars to distract me from my feelings and unhappiness. I found myself confused by how much I didn’t like my college experience given the culture norm of college being the best four years of your life.

During my first year of college, I started talking seriously with a therapist who explained that I might have a mild depression that was making everything feel more difficult and challenging. He explained how medication could help me feel better. I had never liked taking medication, so I focused on the non-medical approaches. I focused on eating healthy, sleeping enough, exercising daily, meditating and trying to find social connections. It was enough to get me through the day, but looking back I never felt happy, and that I was really enjoying my college experience. Rather I was just trying to get through it to move onto the next thing, medical school.

My life continued on like this until my Junior fall when things took a turn for the worse. I started to notice that all of my usually coping strategies weren’t enough. I found myself more apathetic, exhausted, irritable and unable to concentrate in a way I had never experienced before. My therapist began to get very concerned with some of my thoughts and the things I was saying during our meetings. At the same time, I became comfortable with the suicidal ideation that seemed to be constantly circulating through my head, as it was just my new normal.

I finally agreed to see a psychiatrist after I began to get so uncomfortable residing in my own body that I couldn’t see how I would continue onward. As expected, the psychiatrist diagnosed depression, and started me on some medication. I felt different, more numb generally, but not really much better. Fortunately, I was able to get through the end of the semester after cycling through a few different prescriptions, finally finding one that “worked”.

The following semester I went abroad and life was extremely different. I stayed with a host family who took me into their family and loved me as one of their own, something I hadn’t experienced growing up and while at home. I also found strong friendships and connections with peers in my classes. Life was slower paced and I was okay with this. It left me with time to reflect on how I had been living my life up to this point. I was so focused on staying extremely busy in school, that I realized I left no time for people, relationships and real connection. I was living the majority of my college life super busy, isolated and ultimately alone. Although I started to realize that my time spent connecting with people is what brought me more joy and happiness rather than my time spent working on schoolwork.

After months of being on SSRIs, I realized that this medication wasn’t going to fix me, because I wasn’t broken. I had just been living my life basing my value on my ability to achieve which was making me deeply unhappy. I was too focused on what I “should” be doing, rather than what was bringing me joy and happiness. This compounded year over year until nothing made me happy, and so I had to climb out of this deep and dark hole to get where I am now.

I also have learned to feel and process my emotions, as I had grown up in a family that didn’t acknowledge and talk about emotions. As a result I learned to bury feelings and distract myself by staying busy. Consequently, my emotions accumulated over time until they no longer could be contained within myself. I had never learned about mental health or the importance of self-care when I was younger.

I now acknowledge when I’m feeling bad or having a tough day and accept that for what it is, and share honestly with those around me, and with myself. I prioritize my self-care to keep myself in a balanced state mentally.

Sharing your story and feeling heard has been one of the most important things for me. During my heaviest days, just having someone listen to what I was going through proved to be immensely helpful. As if you don’t tell anyone you’ll continue to suffer alone, and nobody deserves that.

One’s mental health is an ever evolving journey. Thank you for learning about mine, and I encourage you to share yours.



Talk. Share. Help.
