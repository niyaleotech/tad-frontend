---
title: Achieve Success in Class
description: Students don’t always have the self-confidence they need to achieve their goals because
img: video_five.jpg
alt: achieve success
author:
  name: Dr
  bio: All about Dr
  img: video_five.jpg
tags:
  - mental health
---

Students don’t always have the self-confidence they need to achieve their goals because they may worry that they won’t measure up to standards (either their own, someone else’s, or society’s) or because they fear competition. You may be standing in our own way!  **If you believe in yourself and your abilities, it is much easier to achieve your goals**.

<span style="color: #1a535c; font-weight: bold;">Attend class every day.</span> Missing school can often mean you will be missing on course lectures, taking notes, participating in class discussions and activities or projects, as well as important dates for assignments, tests, and quizzes. You may be skilled in making up missed work; however, **you can never fully make up all that you miss out on when you are absent**. In order to get good grades and to be most successful, you must attend school every day. If you are absent, your parent or guardian must contact the Attendance Office (310-551-5125) to excuse your absence

<span style="color: #1a535c; font-weight: bold;">Learn how to adapt.</span> Every teacher is different, just like every student is different. Each teacher will have his or her own set of rules, personality, and teaching style. It does not matter if you agree with these rules; it only matters that you follow and adapt to them. Counselors will not change your class if you are not getting along with a teacher. **If you are having difficulty adapting, communicate and work with your teacher or set up a parent meeting to resolve the issue**.

<span style="color: #1a535c; font-weight: bold;">Be prepared.</span> Show up to class every day ready to learn. You need to bring your textbooks, paper, pencils, etc. In order to learn, participate, and to make the most out of every class, you must do your assigned work before each and every class. **Being prepared also means that you come to class physically and mentally alert. In order to do this, you need to eat right, exercise, and get enough sleep.** This will make it easier for you to focus during class.

<span style="color: #1a535c; font-weight: bold;">Chose your seat wisely.</span> It is easier to pay attention in class if you are sitting in an area with limited distractions. **Avoid sitting next to someone who may interrupt you or a friend who might distract you.** You may also want to avoid sitting near windows or doorways that could draw your attention away from what is going on in the classroom. Although some teachers may have assigned seating, if you are having problems paying attention, ask your teacher if it is possible to make a seat change.

<span style="color: #1a535c; font-weight: bold;">Be aware of your body language.</span> Teachers are up in front of the classroom and can see who is paying attention or not. **Teachers can tell who is taking notes and who is listening to the class discussion. They also know which students are doing homework for another class, writing personal notes, or daydreaming** (even when students think that they are getting away with it). If you do any of these things, your teachers may assume that you do not care about their classes and your grade could be lowered. Your body language must communicate to your teachers that you are listening, participating, and learning.

<span style="color: #1a535c; font-weight: bold;">Do your Homework.</span>  **You should always have your homework completed and turned in on time; your grade depends on it!** Get your homework done early in the day by going to the BHHS library at lunch or after school. This is a great stress-reliever, as you can go home feeling calm and relaxed, and then spend more time with family and friends. If you need help with your homework, visit the BHHS Library. Several subjects have tutoring weekly with BHHS teachers.  You should take advantage of these school supports.

<span style="color: #1a535c; font-weight: bold;">Consult with your  school counselor.</span>  When you have questions about any matter concerning you academic program or progress in school. School counselors are available before or after school, during nutrition and lunch, as well as by appointment. See counselor for a list of outside resource, tutors and information regarding credit recovery.


<span style="color: #1a535c; font-weight: bold;">Participate in class.</span>  Many teachers give points based on participation. These points are easy to get and will benefit your grade and class experience. **Participating in class keeps you stay focused and makes the class more interesting and engaging.**

<span style="color: #1a535c; font-weight: bold;">Be a team player.</span> You are often required to do group projects. **Make sure that you are a good group member and that you do your part!** Respect others’ ideas and opinions even if they are different than yours. In college and in your career you will benefit from learning how to be a team player.

<span style="color: #1a535c; font-weight: bold;">Be a respectful classmate.</span>  **Treat your teachers and classmates with respect.** * Don’t talk out of turn. * Don’t put others down or make fun of them. * Pick up after yourself. *Treat others as you would want to be treated. *Be polite. *Listen to your teachers and classmates

(O’Brien)
